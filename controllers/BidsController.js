
/**
 * Created by ali on 5/24/18.
 */

import userModel from '../models/Users';
import bidModel from '../models/Bids';
import matchModel from '../models/Match';
import helpers from '../Helpers/HelperServices';
const userCache = require("../cache");


export default  {
    delete:(req,res)=>{
        console.log(" ======================================= Bid delete ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        console.log("body = "+JSON.stringify(req.body))
        if(req.body.match_id || req.query.match_id ) {
            var user = req.session.user;
            var userId = user.id;

            var match_id = '';
            if(req.body.match_id)match_id=req.body.match_id;
            if(req.query.match_id)match_id=req.query.match_id;

            matchModel.getOne(match_id).then((match)=> {
                if(match.match_status=='pending') {

                    bidModel.delete(user, match_id).then(() => {

                        bidModel.userBidStatus(user.id).then((bidStatus) => {
                            res.send({status: true, message: "bid has been removed",bidStatus:bidStatus})

                        }).catch(() => {
                            res.status(400)
                            res.send({status: false, message: "Invalid bid "})
                        })

                    }).catch((e) => {
                        res.status(400)
                        res.send({status: false, message: "Invalid bid of user "+e})
                    })
                }else{
                    res.status(400)
                    res.send({status:false,message:"You can not remove bid after match start"})

                }
            }).catch(()=>{

                res.status(400)
                res.send({status: false, message: "invalid match id"})

            })
        }else{
            res.status(400)
            res.send({status:false,message:"Missing match Id"})
        }

    },
    bidStatus:(req,res)=>{
        console.log(" ======================================= Plans list ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        var id = req.params.id
        bidModel.bidStatus(id).then((data)=>{
            res.send({status:true,resCode:200,data:data})
        }).catch((e)=>{
            res.send({status:false,message:"some issues please try again later "+e})
        })
    },
    bid:(req,res)=>{
        console.log(" ======================================= Plans list ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        console.log("body = "+JSON.stringify(req.body))
        if(req.body.match_id && req.body.team_id){
            var user = req.session.user;

            var userId = user.id;

            matchModel.getOne(req.body.match_id).then((match)=>{

                if(match.team_a_id== req.body.team_id || req.body.team_id==match.team_b_id) {

                    if(match.match_status=='pending') {
                        bidModel.binOnMatch(req.body.match_id, user, req.body.team_id).then((user) => {
                            req.session.user = user;
                            bidModel.userBidStatus(user.id).then((bidStatus) => {

                                res.send({status: true, resCode: 200,data:"Your bid has been submitted", message: "Your bid has been submitted",bidStatus:bidStatus})
                            }).catch((e) => {
                                res.status(400)
                                res.send({status: false,resCode:400, message: "Can't Bid now " + e})
                            })
                        }).catch((e) => {
                            res.status(400)
                            res.send({status: false,resCode:400, message: "Can't Bid now " + e})
                        })
                    }else{
                        res.status(400)
                        res.send({status:false,message:"You can not bid after match start"})


                    }
                }else{
                    res.status(400)
                    res.send({status:false,message:"Invalid Team Id"})


                }
            }).catch(()=>{

                res.status(400)
                res.send({status:false,message:"Missing match Id"})

            })
        }else{
            res.status(400)
            res.send({status:false,message:"Missing match Id"})
        }
    },
}

