/**
 * Created by ali on 5/22/18.
 */
import userModel from '../models/Users';
import bidModel from '../models/Bids';
import helpers from '../Helpers/HelperServices';
const userCache = require("../cache");
var config = require('../cache').config;

console.log("loading Users");




const users = {

    activation : function(req,res){

        userModel
            .activation(req.params.key)
            .then((u) => {
                res.render('confirmation',{status:true,message:"Your account has been activated"})
            }).catch((e) => {
            res.render('confirmation',{status:false,message:"Invalid Activation key",role:''})

        })

    },
    changepassword: function(req,res){

        userModel
            .changePasswordPage(req.params.key)
            .then((u) => {
                res.render('changePassword',{status:true,msg:""})
            }).catch((e) => {
            res.render('changePassword',{status:false,msg:"Invalid Activation key",role:''})

        })

    },
    updatePassword: function(req,res){

        console.log("Body = "+JSON.stringify(req.body))

        if(req.body.password==req.body.confirm_password) {

            var password = helpers.encrypt(req.body.password.toString());
            userModel
                .changePasswordWithKey(req.params.key, password)
                .then((u) => {
                    res.render('changePassword', {
                        status: false,
                        msg: "Password has been changed successfully",
                        rescode: 200
                    })
                }).catch((e) => {
                res.render('changePassword', {
                    status: false,
                    msg: "Inavlid Request, or expire link ",
                    rescode: 400
                })

            })
        }else{
            res.render('changePassword', {
                status: true,
                msg: "Confirm password not matched",
                rescode: 400
            })

        }
    },
    login:(req,res)=>{
        console.log("==================Login API ====================")
        console.log("================== "+new Date()+" ====================")
        console.log("BODY ="+JSON.stringify(req.body))
        if((req.body.username || req.body.email) && req.body.password) {

            var emailOrUsername = '';
            if(req.body.username) emailOrUsername = req.body.username;
            if(req.body.email) emailOrUsername = req.body.email;
            var ip = helpers.getIpAddress(req);

            helpers.getCountryByIP(ip,function(country) {
                userModel
                    .userByUsername(emailOrUsername)
                    .then((user) => {

                    if(user.active==0){
                        res.status(400);
                        res.send({status: false, resCode: 400, 'message': 'Your email is not verified'});

                    }
                        else if (helpers.decrypt(user.password) == req.body.password) {


                            var todate = helpers.dateFormat(new Date().getTime());
                            var userLogin = helpers.dateFormat(user.lastLogin);

                            var coins = parseInt(user.coins);
                            console.log(userLogin +" == "+todate)
                            if(userLogin!=todate){
                                if(config.dailyLogin && config.dailyLogin>0){
                                    coins = coins + parseInt(config.dailyLogin);
                                }
                            }
                            var token = helpers.md5(new Date().getTime().toString());
                            if (req.body.deviceId && req.body.device_token && req.body.platform) {
                                var updateUser = {
                                    token: token,
                                    ip: ip,
                                    coins:coins,
                                    country:country.country,
                                    timezone:country.timezone,
                                    deviceId: req.body.deviceId,
                                    device_token: req.body.device_token,
                                    platform: req.body.platform,
                                    lastLogin: new Date().getTime()
                                }
                                console.log(updateUser)
                                userModel.updateDevice(user.id, updateUser);
                            } else {
                                var token = helpers.md5(new Date().getTime().toString());
                                var updateUser = {
                                    token: token,
                                    coins:coins,
                                    country:country.country,
                                    timezone:country.timezone,
                                    ip: ip,
                                    lastLogin: new Date().getTime()
                                }

                                console.log(updateUser)
                                userModel.updateTokenLogin(user.id, updateUser);

                            }

                            delete user.password;
                            delete user.active;
                            bidModel.userBidStatus(user.id).then((bidStatus) => {
                                user.token = token;
                                userCache.userToken[token] = user.id;
                                userCache.users[user.id] = user;
                                res.send({
                                    status: true,
                                    resCode: 200,
                                    message: "successfully login",
                                    data: user,
                                    bidStatus: bidStatus
                                })
                            }).catch((bidStatus) => {
                                userCache.userToken[token] = user.id;
                                userCache.users[user.id] = user;
                                res.send({
                                    status: true,
                                    resCode: 200,
                                    message: "successfully login",
                                    data: user,
                                    bidStatus: bidStatus,
                                })

                            })
                        } else {
                            res.status(400);
                            res.send({status: false, resCode: 400, 'message': 'Incorrect password'});

                        }


                    }).catch((e) => {
                    console.log('Empty')
                    res.status(400);
                    res.send({status: false, resCode: 400, 'message': 'Email address not verified or '+e});

                })
            })

        }else{
            res.status(400);
            res.send({status:false,resCode:400,'message':'Incomplete information'});

        }


        },
    register:(req,res)=>{
        console.log("==================Register API ====================")
        console.log("================== "+new Date()+" ====================")
        console.log("BODY ="+JSON.stringify(req.body))
        if(req.body.email && req.body.username && req.body.password){
            var coins = 0;
            if(config.onRegister){
                coins = config.onRegister;
            }
            let newUser = {
                password : helpers.encrypt(req.body.password.toString()),
                email:req.body.email,
                username:req.body.username,
                active:0,
                keys: helpers.md5(new Date().getTime().toString()),
                points:0,
                coins:coins,
                balance:0,
                token:'',
                lastLogin:new Date().getTime(),
            };
            userModel
                .register(newUser)
                .then(function () {
                    res.status(200);
                    helpers.newUserActivationEmail(newUser,req.headers.host,req.body.password)
                res.send({'status':true,resCode:200,message:"Email Verification has been sent"});
                }).catch(function (data){
                    var error = '';
                    if(data.email==req.body.email){
                        error = 'Email address already exists';
                    }else{
                        error = 'Username already exists';

                    }
                res.status(400);
                res.send({'status':true,resCode:400,message:error});

            })

        }else{
            res.status(400);
            res.send({status:false,resCode:400,'message':'Incomplete information'});

        }

    },
    socialLogin:(req,res)=>{
        console.log("================== Social Login ====================")
        console.log("================== "+new Date()+" ====================")
        console.log("BODY ="+JSON.stringify(req.body))
        if(req.body.email && req.body.network && req.body.networkId && req.body.username){
           var network = ['facebook','google','twitter','gmail']
            if(network.indexOf(req.body.network)!=-1) {

                var coins = 0;

                var ip = helpers.getIpAddress(req);

                helpers.getCountryByIP(ip,function(country) {

                    let newUser = {
                        email: req.body.email,
                        networkId: req.body.networkId,
                        network: req.body.network,
                        country:country.country,
                        timezone:country.timezone,
                        ip:ip,
                        username: req.body.username,
                        active: 1,
                        keys: helpers.encrypt(new Date().getTime().toString()),
                        points: 0,
                        coins: coins,
                        lastLogin: new Date().getTime(),
                        balance: 0,
                        password: Math.random().toString(36).slice(-6),
                        token: helpers.md5(new Date().getTime().toString()),
                    };
                    if (req.body.deviceId && req.body.device_token && req.body.platform) {
                        newUser['deviceId'] = req.body.deviceId
                        newUser['device_token'] = req.body.device_token
                        newUser['deviceId'] = req.body.deviceId
                        newUser['platform'] = req.body.platform
                    }else{

			//  newUser['deviceId'] = req.body.deviceId
                        newUser['device_token'] = '';
                        newUser['deviceId'] = '';
                        newUser['platform'] = '';
}
if(req.body.deviceId) newUser.deviceId = req.body.deviceId;
if(req.body.platform) newUser.platform = req.body.platform;
if(req.body.device_token) newUser.device_token = req.body.device_token;






                    userModel
                        .socialLogin(newUser,req.headers.host)
                        .then((user) => {
                            res.status(200);
                            var resp = {
                                id: user.id,
                                username: user.username,
                                email: user.email,
                                balance: user.balance,
                                points: user.points,
                                coins: user.coins,
                                token: user.token,
                            }
                            userCache.userToken[resp.token] = resp.id;
                            userCache.users[resp.id] = resp;
                            bidModel.userBidStatus(user.id).then((bidStatus) => {
                                res.send({
                                    'status': true,
                                    resCode: 200,
                                    message: "Successfully login",
                                    'user': resp,
                                    bidStatus: bidStatus
                                });
                            }).catch((bidStatus) => {
                                res.send({
                                    'status': true,
                                    resCode: 200,
                                    message: "Successfully login",
                                    'user': resp,
                                    bidStatus: bidStatus
                                })
                            })
                        }).catch(() => {
                        res.status(400);
                        res.send({'status': true, resCode: 400, message: "Email Address already exist"});

                    })
                });
           }else{

               res.status(400);
               res.send({status:false,resCode:400,'message':'Unknown login authentication'});

           }
        }else{
            res.status(400);
            res.send({status:false,resCode:400,'message':'Incomplete information'});

        }

    },
    changePassword:(req,res)=>{
        console.log("================== Change Password ====================")
        console.log("================== "+new Date()+" ====================")
        console.log("BODY ="+JSON.stringify(req.body))
        if(req.body.password && req.body.oldPassword){
            var user = req.session.user;
            userModel.userById(user.id).then((user)=>{

            if(req.body.oldPassword==helpers.decrypt(user.password))
            {
                var password = helpers.encrypt(req.body.password.toString());
                userModel
                    .changePassword(user.id,password)
                    .then(()=>{
                        res.status(200);
                        res.send({'status':true,resCode:200,message:"Password has been updated"});
                    }).catch(()=>{
                    res.status(400);
                    res.send({'status':true,resCode:400,message:"Unable to update password, please login again"});

                })

            }else{
                res.status(400);
                res.send({status:false,resCode:400,'message':'Wrong old password'});

            }
            }).catch((e)=>{
                res.status(400);
                res.send({status:false,resCode:400,'message':e});

            })
        }else{
            res.status(400);
            res.send({status:false,resCode:400,'message':'Incomplete information'});

        }

    },
    update:(req,res)=>{
        console.log("================== Change Password ====================")
        console.log("================== "+new Date()+" ====================")
        console.log("BODY ="+JSON.stringify(req.body))
        if(req.body.name && req.body.phone){
            var user = req.session.user;
            userModel.userById(user.id).then((user)=>{
                helpers.uploadImagebyName(req,'image',function(img){
                    var updateObj = {name:req.body.name,phone:req.body.phone,image:''};
                   if(user.image )
                       updateObj.image = user.image;
                    if(img){
                        updateObj.image = img;
                    }
                  userModel
                    .update(updateObj,user.id)
                    .then(()=>{
                        res.status(200);
                        bidModel.userBidStatus(user.id).then((bidStatus) => {
                            res.send({'status': true, resCode: 200, message: "Successfully updated",bidStatus:bidStatus});
                        }).catch((e)=>{
                            res.status(400);
                            res.send({'status':true,resCode:400,message:"Unable to update profile, please login again "+e});

                        })

                    }).catch((e)=>{
                    res.status(400);
                    res.send({'status':true,resCode:400,message:"Unable to update profile, please login again "+e});
                    });
                })
            }).catch((e)=>{
                res.status(400);
                res.send({status:false,resCode:400,'message':'unable to update '+e});

            })
        }else{
            res.status(400);
            res.send({status:false,resCode:400,'message':'Incomplete information'});

        }

    },
    forgotPassword:(req,res)=>{
        console.log("================== Forgot Password ====================")
        console.log("================== "+new Date()+" ====================")
        console.log("BODY ="+JSON.stringify(req.body))
        var emailOrUsername = '';
        if(req.body.username) emailOrUsername = req.body.username;
        if(req.body.email) emailOrUsername = req.body.email;
        userModel
            .forgotPassword(emailOrUsername)
            .then((user) => {
            helpers.forgotPassword(user,req.headers.host,'')
                res.status(200);
                res.send({'status':true,resCode:200,message:"You will received email shortly"});
            }).catch((e)=>{
            res.status(400);
            res.send({status:false,resCode:400,'message':'Invalid login id ' +e});

        })
    },
    me:(req,res)=>{
        console.log("================== ME ====================")
        console.log("================== "+new Date()+" ====================")
        console.log("BODY ="+JSON.stringify(req.body))
        var user = req.session.user;

		 userModel
                    .userByUsername(user.email)
                    .then((user) => {

		var name = '';
		var image = '';
		var phone = '';
		if(user.phone) phone = user.phone;
		if(user.name) name = user.name;
		if(user.image) image = user.image;
            var resp = {
                id: user.id,
                username: user.username,
                email: user.email,
		image:image,
		phone:phone,
		name:name,
                balance: user.balance,
                points: user.points,
                coins: user.coins,
                token: user.token,
            }
        bidModel.userBidStatus(user.id).then((bidStatus)=> {

            res.send({'status': true, resCode: 200, message: "Successfully login", 'user': resp,bidStatus:bidStatus});
        }).catch((bidStatus)=>{
            res.send({'status': true, resCode: 200, message: "Successfully login", 'user': resp,bidStatus:bidStatus});

        })
	}).catch((bidStatus)=>{
            res.send({'status': true, resCode: 200, message: "Successfully login", 'user': user,bidStatus:{}});

        })

    },
    ranking:(req,res)=>{
        console.log("================== ME ====================")
        console.log("================== "+new Date()+" ====================")
        var user = req.session.user;
        var sportId = req.params.sport_id;

        if(!req.query.league_id) {
            bidModel.ranking(sportId).then((data) => {
                res.send({
                    status: true,
                    resCode: 200,
                    data: data
                })


            }).catch((e) => {

                res.send({
                    status: true,
                    resCode: 200,
                    data: [],
                    message: e
                })

            })
        }else{
            bidModel.rankingByLeagueId(sportId,req.query.league_id).then((data) => {
                res.send({
                    status: true,
                    resCode: 200,
                    data: data
                })


            }).catch((e) => {

                res.send({
                    status: true,
                    resCode: 200,
                    data: [],
                    message: e
                })

            })
        }


    },
    logout:(req,res)=>{
        console.log("================== LOGOUT  ====================")
        console.log("================== "+new Date()+" ====================")
        var user = req.session.user;
        delete userCache.userToken[user.token];
        var token = helpers.md5(new Date().getTime().toString());
        var updateUser = {
            token :token,
            lastLogin :new Date().getTime()
        }

        userModel.updateToken(user.id,updateUser);

        res.send({
            status:true,
            resCode:200,
            data:'thank you for using this application'
        })


    },
};
console.log("completed Users");

export default users;
