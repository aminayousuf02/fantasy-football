/**
 * Created by ali on 5/24/18.
 */
/**
 * Created by ali on 5/24/18.
 */

import userModel from '../models/Users';
import seriesModel from '../models/Series';
import matchModel from '../models/Match';
import helpers from '../Helpers/HelperServices';
const userCache = require("../cache");


export default  {
    leagues:(req,res)=>{
        console.log(" ======================================= Series list ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        if(req.query.sport_id) {
            if(req.session.user){
                var sport_id = req.query.sport_id;
                var user = req.session.user;
                seriesModel.getAllBySportIdAndUserId(sport_id,user.id).then((data) => {
                    res.send({status: true, resCode: 200, data: data})
                }).catch((e) => {
                    res.send({status: false, message: e})

                })

            }else {
                var sport_id = req.query.sport_id;
                seriesModel.getAllBySportId(sport_id).then((data) => {
                    res.send({status: true, resCode: 200, data: data})
                }).catch(() => {
                    res.send({status: false, message: "some issues please try again later"})

                })
            }
        }else if(req.query.leagueId){

                matchModel.getByLeagueId(req.query.leagueId,req.session.user).then((data)=>{
                    res.send({status: true, resCode: 200, data: data})

                }).catch(()=>{
                    res.send({status: true, resCode: 200, data: []})

                })

        }else{
            var sport_id = req.query.sport_id;
            seriesModel.getAll().then((data) => {
                res.send({status: true, resCode: 200, data: data})
            }).catch(() => {
                res.send({status: false, message: "some issues please try again later"})

            })

        }
    },
    leaguesAdmin:(req,res)=>{
        console.log(" ======================================= Series list ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        if(req.query.sport_id) {
            var sport_id = req.query.sport_id;
            seriesModel.getAllBySportId(sport_id).then((data) => {
                res.send({status: true, resCode: 200, data: data})
            }).catch(()=>{
                res.send({status:false,message:"some issues please try again later"})

            })
        }else{
            seriesModel.getAll().then((data) => {
                res.send({status: true, resCode: 200, data: data})
            }).catch(()=>{
                res.send({status:false,message:"some issues please try again later"})

            })

        }
    },
    getOne:(req,res)=>{
        console.log(" ======================================= Series list ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        var id = req.params.id
        seriesModel.getOne(id).then((data)=>{
            res.send({status:true,resCode:200,data:data})
        }).catch(()=>{
            res.send({status:false,message:"some issues please try again later"})

        })

    },
    saveLeague:(req,res)=>{
        console.log(" ======================================= Series Save ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        console.log(" Body = "+JSON.stringify(req.body))

        if(req.body.title) {
            helpers.uploadImagebyName(req, 'image_inactive', function (image_inactive) {
                helpers.uploadImagebyName(req, 'image_active', function (image_active) {

                    var league = {
                        title: req.body.title,
                        start_date: new Date(req.body.start_date).getTime(),
                        end_date: new Date(req.body.end_date).getTime(),
                        league_status: req.body.status,
                        sport_id: req.body.sport_id,
                        image_active: image_active,
                        color: req.body.color,
                        image_inactive: image_inactive,
                    }

                    seriesModel.save(league).then(() => {
                        res.send({"status": true, message: "Leagues has been saved"})
                    }).catch((e)=>{
                        res.send({status:false,message:"some issues please try again later "+e})

                    })

                })
            })

        }else{
            res.status(400)
            res.send({status:false,message:"Missing required params"})
        }
    },
    deleteLeague:(req,res)=>{
        console.log(" ======================================= Series delete ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        var id = req.params.id;
        seriesModel.delete(id).then(()=>{
            res.send({status:true,message:"League has been deleted"})

        }).catch(()=>{
            res.send({status:false,message:"some issues please try again later"})

        })

    },
    update:(req,res)=>{
        console.log(" ======================================= Series Save ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        console.log(" Body = "+JSON.stringify(req.body))
        var id = req.params.id;
        seriesModel.getOne(id).then((series)=> {
            helpers.uploadImagebyName(req, 'image_inactive', function (image_inactive) {
                helpers.uploadImagebyName(req, 'image_active', function (image_active) {
                    if(image_active){
                        image_active = image_active;
                    }else{
                        image_active = series.image_active;
                    }

                    if(image_inactive){
                        image_inactive = image_inactive;
                    }else{
                        image_inactive = series.image_inactive;
                    }

                    var league = {
                        title: req.body.title,
                        start_date: new Date(req.body.start_date).getTime(),
                        end_date: new Date(req.body.end_date).getTime(),
                        league_status: req.body.status,
                        sport_id: req.body.sport_id,
                        image_active: image_active,
                        image_inactive: image_inactive,
                    }

                    seriesModel.update(id,league).then(() => {
                        res.send({"status": true, message: "Leagues has been updated"})
                    }).catch(()=>{
                        res.status(400)
                        res.send({status:true,resCode:400,message:"Unable to updated"})

                    })

                })
            })
        }).catch(()=>{
            res.status(400)
            res.send({status:true,resCode:400,message:"Unable to updated"})

        })

    },
    joinLeague:(req,res)=>{
        console.log(" ======================================= join League ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        console.log(" Body = "+JSON.stringify(req.body))
        if(req.body.league_id) {
            var user = req.session.user;
            var leagueId = req.body.league_id;
            var userId = user.id;
            seriesModel.joinLeague(userId,leagueId).then((series) => {

                res.send({
                    status:true,
                    resCode:200,
                    message:"You have joined the league successfully"
                })

            }).catch((e) => {
                res.status(400)
                res.send({status: true, resCode: 400, message: e})
            })
        }else{
            res.status(400)
            res.send({status:false,message:"Missing required params"})
        }

    },
    leaveLeague:(req,res)=>{
        console.log(" ======================================= join League ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        console.log(" Body = "+JSON.stringify(req.body))
        if(req.body.league_id) {
            var user = req.session.user;
            var leagueId = req.body.league_id;
            var userId = user.id;
            seriesModel.leaveLeague(userId,leagueId).then((series) => {

                res.send({
                    status:true,
                    resCode:200,
                    message:"you have left this group"
                })

            }).catch((e) => {
                res.status(400)
                res.send({status: true, resCode: 400, message: e})
            })
        }else{
            res.status(400)
            res.send({status:false,message:"Missing required params"})
        }

    },
}

