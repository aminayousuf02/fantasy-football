/**
 * Created by ali on 6/5/18.
 */
/**
 * Created by ali on 5/27/18.
 */
/**
 * Created by ali on 5/24/18.
 */
/**
 * Created by ali on 5/24/18.
 */

import userModel from '../models/Users';
import orderModel from '../models/Orders';
import planModel from '../models/Plans';
import helpers from '../Helpers/HelperServices';
const userCache = require("../cache");


export default  {
    saveMatch:(req,res)=>{
        console.log(" ======================================= Team Match ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        console.log(" Body = "+JSON.stringify(req.body))
        if(req.body.team_a_id && req.body.team_b_id && req.body.series_id && req.body.start_date && req.body.end_date && req.body.venue && req.body.sport_id) {
            var group_info = '';
            if(req.body.group_info) group_info = req.body.group_info;

            var match = {
                team_b_id: req.body.team_b_id,
                team_a_id: req.body.team_a_id,
                series_id: req.body.series_id,
                sport_id: req.body.sport_id,
                start_date: new Date(req.body.start_date).getTime(),
                end_date: new Date(req.body.end_date).getTime(),
                start_time:req.body.start_time,
                timezone:req.body.timezone,
                venue: req.body.venue,
                result: '',
                match_status: 'pending',
                team_a_score: '0',
                stronger_team_id: req.body.stronger_team_id,
                team_b_score: '0',
                group_info: group_info,
            }

            matchModel.save(match).then(() => {
                res.send({"status": true, message: "Match has been saved"})
            }).catch(()=>{
                res.send({status:false,message:"some issues please try again later"})

            })


        }else{
            res.status(500)
            res.send({status:false,message:"Missing required params"})
        }

    },
    update:(req,res)=>{
        console.log(" ======================================= Update Match ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        console.log(" Body = "+JSON.stringify(req.body))
        var id = req.params.id;

        var group_info = '';
        if(req.body.group_info) group_info = req.body.group_info;

        var match = {
            team_b_id: req.body.team_b_id,
            team_a_id: req.body.team_a_id,
            series_id: req.body.series_id,
            sport_id: req.body.sport_id,
            start_date: new Date(req.body.start_date).getTime(),
            end_date: new Date(req.body.end_date).getTime(),
            start_time:req.body.start_time,
            stronger_team_id: req.body.stronger_team_id,
            timezone:req.body.timezone,
            venue: req.body.venue,
            result: '',
            match_status: 'pending',
            team_a_score: '0',
            team_b_score: '0',
            group_info: group_info,
        }

        matchModel.update(id,match).then(() => {
            res.send({"status": true, message: "Match has been updated"})
        }).catch((e)=>{
            res.send({status:true,resCode:500,message:"Unable to updated "+e})

        })



    },
    delete:(req,res)=>{
        console.log(" ======================================= Team delete ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        var id = req.params.id;
        matchModel.delete(id).then(()=>{
            res.send({status:true,message:"Match has been deleted"})

        }).catch(()=>{
            res.send({status:false,message:"some issues please try again later"})

        })

    },
    getAll:(req,res)=>{
        console.log(" ======================================= Plans list ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        var id = req.params.id
        planModel.getAll().then((data)=>{
            res.send({status:true,resCode:200,data:data})
        }).catch(()=>{
            res.status(400);
            res.send({status:false,message:"some issues please try again later"})
        })
    },
    purchase:(req,res)=>{
        console.log(" ======================================= Purchase Plan API ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        console.log(" BODY = "+JSON.stringify(req.body))
        if(req.body.plan_id){
            var user = req.session.user;
            console.log(user)
            planModel.getOne(req.body.plan_id).then((data)=>{
                userModel.userById(user.id).then((user)=>{
                    req.session.user = user;

                    var coins = parseInt(data.coins)+parseInt(user.coins);
                userModel.updateCoins(coins,user.id).then(()=>{
                    var orderRef = '';
                    if(req.body.orderRef){
                        orderRef = req.body.orderRef;
                    }
                    var order = {
                        plan_id:data.id,
                        user_id:user.id,
                        amount:data.price,
                        currency:data.currency,
                        order_ref:orderRef,
                        dated:helpers.dateAndTimeFormat(new Date()),
                        order_status:'approved',
                    }
                    orderModel.save(order).then(()=>{
                        req.session.user.coins +=data.coins
                        console.log("req.session.user.coins = " +req.session.user.coins)
                        res.status(200);
                        res.send({status:true,resCode:200,message:" "+data.coins+" coins has been added to your account"})
                    }).catch(()=>{

                        res.status(400);
                        res.send({status:false,resCode:400,message:"Order not completed"})
                    })

                    }).catch(()=>{
                        res.status(400);
                        res.send({status:false,resCode:400,message:"Order not completed"})

                    })



                }).catch(()=>{
                    res.status(400);
                    res.send({status:false,resCode:400,message:"Order not completed"})

                })

            }).catch((e)=>{
                res.status(400);
                res.send({status:false,resCode:400,message:"Invalid plan id "+e})

            })


        }else{
            res.status(400);
            res.send({status:false,resCode:400,message:"missing plan id"})
        }
    }
}

