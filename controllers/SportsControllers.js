/**
 * Created by ali on 5/22/18.
 */
import sportModel from '../models/Sports';

const sports = {
    getAll:(req,res)=>{
        sportModel.getAll()
            .then(function(data){
            res.send({
                status:true,
                resCode:200,
                data:data
            })
        }).catch(function () {
            res.send({
                status:false,
                resCode:400,
                data:[]
            })

        })
    }
}
export default sports;
