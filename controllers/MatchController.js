/**
 * Created by ali on 5/27/18.
 */
/**
 * Created by ali on 5/24/18.
 */
/**
 * Created by ali on 5/24/18.
 */

import userModel from '../models/Users';
import matchModel from '../models/Match';
import helpers from '../Helpers/HelperServices';
const userCache = require("../cache");


export default  {
    getByLeagueId:(req,res)=>{
        if(req.query.leagueId ||req.query.league_id) {
            console.log("getByLeagueId works")
            var user = req.session.user;
            var leagueId = null;
            if(req.query.leagueId) leagueId = req.query.leagueId;
            if(req.query.league_id) leagueId = req.query.league_id;

            matchModel.getByLeagueId(leagueId,user).then((data) => {
                res.send({status:true,resCode:200,data:data})
            }).catch(() => {
                res.send({status:true,resCode:200,data:[]})
            })
        }
        else{
            console.log("getByLeagueId works")
            matchModel.getAll().then((data) => {
                for (var i = 0; i < data.length; i++) {
                    console.log(data[i].start_date);
                }
                res.send({status:true,resCode:200,data:data})
            }).catch(() => {
                res.send({status:true,resCode:200,data:[]})
            })
        }

    },
    getFixture:(req,res)=>{
            var user = req.session.user;
            var leagueId = null;
            if(req.query.leagueId) leagueId = req.query.leagueId;
            if(req.query.league_id) leagueId = req.query.league_id;
        matchModel.getFixture(user,leagueId).then((data) => {
                res.send({status:true,resCode:200,data:data})
            }).catch(() => {
                res.send({status:true,resCode:200,data:[]})
            })
    },
    saveMatch:(req,res)=>{
        console.log(" ======================================= Team Match  ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        console.log(" Body = "+JSON.stringify(req.body))
        if(req.body.team_a_id && req.body.team_b_id && req.body.series_id && req.body.start_date && req.body.end_date && req.body.venue && req.body.sport_id) {
                var group_info = '';
                if(req.body.group_info) group_info = req.body.group_info;

                var startDate = new Date(req.body.start_date);
                startDate.setHours(0)
                startDate.setMinutes(0)
                startDate.setSeconds(0)
                console.log(helpers.dateFormat(startDate))
                var video_link = '';
                if(req.body.video_link){
                    video_link = req.body.video_link;
                }
                var audio_link = '';
                if(req.body.audio_link){
                    audio_link = req.body.audio_link;
                }
                console.log("audio_link");
                console.log(audio_link);
                var match = {
                    team_b_id: req.body.team_b_id,
                    team_a_id: req.body.team_a_id,
                    series_id: req.body.series_id,
                    sport_id: req.body.sport_id,
                    start_date: new Date(startDate ).getTime() + (parseInt(req.body.start_time)*60*1000),
                    end_date: new Date(req.body.end_date).getTime(),
                    start_time:req.body.start_time,
                    timezone:req.body.timezone,
                    venue: req.body.venue,
                    result: '',
                    video_link: video_link,
                    audio_link: audio_link,
                    match_status: 'pending',
                    team_a_score: '0',
                    stronger_team_id: req.body.stronger_team_id,
                    team_b_score: '0',
                    group_info: group_info,
                     }

                    matchModel.save(match).then(() => {
                    res.send({"status": true, message: "Match has been saved"})
                }).catch(()=>{
                        res.send({status:false,message:"some issues please try again later"})

                    })


        }else{
            res.status(500)
            res.send({status:false,message:"Missing required params"})
        }

    },
    update:(req,res)=>{
        console.log(" ======================================= Update Match ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        console.log(" Body = "+JSON.stringify(req.body))
        var id = req.params.id;

            var group_info = '';
            if(req.body.group_info) group_info = req.body.group_info;


        var startDate = new Date(req.body.start_date);
        startDate.setHours(0)
        startDate.setMinutes(0)
        startDate.setSeconds(0)
        console.log(new Date(new Date(startDate ).getTime()+(parseInt(req.body.start_time)*60*1000)))
        var video_link = '';
        if(req.body.video_link){
            video_link = req.body.video_link;
        }
        var audio_link = '';
        if(req.body.audio_link){
            audio_link = req.body.audio_link;
        }

        var match = {
                team_b_id: req.body.team_b_id,
                team_a_id: req.body.team_a_id,
                series_id: req.body.series_id,
                sport_id: req.body.sport_id,
                start_date: new Date(startDate ).getTime() + (parseInt(req.body.start_time)*60*1000),
                end_date: new Date(req.body.end_date).getTime(),
                start_time:req.body.start_time,
                stronger_team_id: req.body.stronger_team_id,
                timezone:req.body.timezone,
                venue: req.body.venue,
                result: '',
                video_link: video_link,
                audio_link: audio_link,
                match_status: 'pending',
                team_a_score: '0',
                team_b_score: '0',
                group_info: group_info,
            }

                    matchModel.update(id,match).then(() => {
                        res.send({"status": true, message: "Match has been updated"})
                    }).catch((e)=>{
                        res.send({status:true,resCode:500,message:"Unable to updated "+e})

                    })



    },
    matches_scoreUpdate:(req,res)=>{
        console.log(" ======================================= Update Match Completed ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        console.log(" Body = "+JSON.stringify(req.body))
        var id = req.params.id;

            var group_info = '';

            var match = {
                team_win_id: req.body.team_win_id,
                team_a_score: req.body.team_a_score,
                team_b_score: req.body.team_b_score,
                result: req.body.result,
                isLive: req.body.isLive,
                match_status: req.body.match_status,
            }

                matchModel.updateCoins(id,req.body,function() {
                    matchModel.scoreUpdate(id, match).then(() => {

                        res.send({"status": true, message: "Match has been updated"})
                    }).catch((e) => {
                        res.send({status: true, resCode: 500, message: "Unable to updated " + e})

                    })

                })

    },
    delete:(req,res)=>{
        console.log(" ======================================= Team delete ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        var id = req.params.id;
        matchModel.delete(id).then(()=>{
            res.send({status:true,message:"Match has been deleted"})

        }).catch(()=>{
            res.send({status:false,message:"some issues please try again later"})

        })

    },
    getOne:(req,res)=>{
        console.log(" ======================================= Series list ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        var id = req.params.id
        matchModel.getOne(id).then((data)=>{
            res.send({status:true,resCode:200,data:data})
        }).catch(()=>{
            res.send({status:false,message:"some issues please try again later"})
        })
    },
}

