/**
 * Created by ali on 5/24/18.
 */

import userModel from '../models/Users';
import helpers from '../Helpers/HelperServices';
const userCache = require("../cache");


export default  {
    index:(req,res)=>{
        var user = req.session.user;
        res.render('admin/index',{user:user})
    },
    login:(req,res)=>{
        console.log(" ======================================= Admin login page ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        res.render("admin/login",{status:true,message:""})
    },
    loginPost:(req,res)=>{
        console.log(" ================== Admin Login Request  ===================")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        console.log("BODY = "+JSON.stringify(req.body))
        if(req.body.email && req.body.password) {

            userModel.userByEmail(req.body.email)
                .then((user)=>{
                    if(helpers.decrypt(user.password)==req.body.password){
                        if(user.role=='admin'){

                            req.session.user = user;

                            res.redirect("/admin")

                        }else {
                            res.render("admin/login", {status: false, message: "Email address not exist"})
                        }

                    }else{
                        res.render("admin/login", {status: false, message: "Invalid Password"})

                    }
                }).catch(()=>{
                res.render("admin/login", {status: false, message: "Invalid Email address"})
                })


        }else{
            res.render("admin/login", {status: false, message: "Missing required Parameter"})

        }
    },
    users:(req,res)=>{
      userModel.getAll.then((data)=>{
          res.send({status:true,data:data})
      })
    },
}

