/**
 * Created by ali on 5/27/18.
 */
/**
 * Created by ali on 5/24/18.
 */
/**
 * Created by ali on 5/24/18.
 */

import userModel from '../models/Users';
import teamModel from '../models/Team';
import helpers from '../Helpers/HelperServices';
const userCache = require("../cache");


export default  {

    league_teams:(req,res)=>{
        console.log(" ======================================= Team Save ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        console.log(" Body = "+JSON.stringify(req.body))

        if(req.body.teams && req.body.leagueId){

            teamModel.teamAddOnLeague(req.body.teams,req.body.leagueId).then(()=>{

                res.send({status:true,message:"Teams added on leagues"})

            }).catch(()=>{
                res.send({status:false,message:"Unable to save"})

            })


        }else{
            res.send({status:false,message:"Missing required params"})

        }

    },
    getAllBytournamentId:(req,res)=>{
        console.log(" ===================== getAllBytournamentId ===================")
        console.log(" ===================== "+new Date()+" ===================")
        var leagueId = req.query.leagueId;
        teamModel.getAllBytournamentId(leagueId)
            .then((data)=>{
            res.send({status:true,resCode:200,data:data})
            }).catch(()=>{
            res.send({status:true,resCode:200,data:[]})

            })
    },
    delete:(req,res)=>{
        console.log(" ======================================= Team delete ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        var id = req.params.id;
        teamModel.delete(id).then(()=>{
            res.send({status:true,message:"League has been deleted"})

        })

    },
    getAll:(req,res)=>{
        console.log(" ===================== Get Team by Sport Id ===================")
        console.log(" ===================== "+new Date()+" ===================")

        if(!req.query.sport_id) {
            teamModel.getAll()
                .then((data) => {
                    res.send({status: true, resCode: 200, data: data})
                }).catch(() => {
                res.send({status: true, resCode: 200, data: []})

            })
        }else{

            teamModel.getAllBySportId(req.query.sport_id)
                .then((data) => {

                    res.send({status: true, resCode: 200, data: data})
                }).catch(() => {
                res.send({status: true, resCode: 200, data: []})

            })
        }
    },
    saveTeam:(req,res)=>{
        console.log(" ======================================= Team Save ======================================= ")
        console.log(" ======================================= "+new Date()+" ======================================= ")
        console.log(" Body = "+JSON.stringify(req.body))

        if(req.body.name && req.body.sport_id ) {
            helpers.uploadImagebyName(req, 'image', function (image) {

                var country = '';
                if(req.body.country) country = req.body.country;
                    var team = {
                        name: req.body.name,
                        sport_id : req.body.sport_id ,
                        image: image,
                        country: country,
                    }

                    teamModel.save(team).then(() => {
                        res.send({"status": true, message: "Team has been saved"})
                    })

                })

        }else{
            res.status(500)
            res.send({status:false,message:"Missing required params"})
        }
    },
 }

