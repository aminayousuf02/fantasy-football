/**
 * Created by ali on 10/7/18.
 */
import bannerModel from '../models/Banner';

const BANNER = {
    getAll:(req,res)=>{
        let seriesId = null
        if (req.query.seriesId) seriesId = req.query.seriesId;
        bannerModel.getAll(seriesId)
            .then(function(data){
                res.send({
                    status:true,
                    resCode:200,
                    data:data
                })
            }).catch(function () {
            res.send({
                status:false,
                resCode:400,
                data:[]
            })

        })
    }
}
export default BANNER;
