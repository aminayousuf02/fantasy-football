import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import Debug from 'debug';
import express from 'express';
import session from 'express-session';
import multer from 'multer';
import config from './models/Config';
var argv = require('minimist')(process.argv.slice(2));

import logger from 'morgan';
import path from 'path';
// import favicon from 'serve-favicon';
import userModel from './models/Users';

import index from './routes/index';
import api from './routes/api';
import admin from './routes/admin';

/*
var swagger = require("swagger-node-express");

var swaggerUi = require('swagger-ui-express'),
    swaggerDocument = require('./swagger.json');
*/

const app = express();
const debug = Debug('fantasy-football:app');
app.set('views', path.join(__dirname, 'views'));
// view engine setup
app.use(multer({dest:'./public/uploads/'}).any());

app.set('view engine', 'ejs');
// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(errorHandler);
app.use(session({ secret : '1234567890QWERTY' }));

userModel.loadAllUser();
userModel.checkAdmin();
function errorHandler(err, req, res, next) {

// XHR Request?
    if (req.xhr) {
        logger.error(err);
        res.status(500).send({ error: 'Internal Error Occured.' });
        return;
    }

// Not a XHR Request.
    logger.error(err);
    res.status(500);
    res.render('framework/error', { error: "Internal Server Error." });

// Note: No need to call next() as the buck stops here.
    return;
}

app.use(bodyParser.urlencoded({
  extended: false
}));

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}
app.use(allowCrossDomain);
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json({limit: '500mb'}));
app.use(bodyParser.urlencoded({limit: '500mb', extended: true}));
/*
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
*/

app.use('/', index);
app.use('/api/v1', api);
app.use('/admin', admin);
/*
swagger.setAppHandler(api);
swagger.setApiInfo({
    title: "example API",
    description: "API to do something, manage something...",
    termsOfServiceUrl: "",
    contact: "yourname@something.com",
    license: "",
    licenseUrl: ""
});
*/
app.get('/api-documents', function (req, res) {
    res.sendfile(__dirname + '/dist/index.html');
});
/*
swagger.configureSwaggerPaths('', 'api-docs', '');
*/

config.getAll().then((data)=>{
    console.log(data)
}).catch(()=>{
    console.log("Empty Configuration")
})
var domain = 'localhost';
if(argv.domain !== undefined)
    domain = argv.domain;
else
    console.log('No --domain=xxx specified, taking default hostname "localhost".');
var applicationUrl = 'http://' + domain;
console.log(applicationUrl)
/*
swagger.configure(applicationUrl, '1.0.0');
*/

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
/* eslint no-unused-vars: 0 */
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// Handle uncaughtException
process.on('uncaughtException', (err) => {
 console.log(err)
  debug('Caught exception: %j', err);
  process.exit(1);
});

export default app;
