import express from 'express';

const router = express.Router();
import usersControllers from '../controllers/UserControllers'
import SportsControllers from '../controllers/SportsControllers'
import seriesController from '../controllers/seriesController'
import MatchController from '../controllers/MatchController'
import TeamTontroller from '../controllers/TeamController'
import PlansControllers from '../controllers/PlansControllers'
import ConfigController from '../controllers/ConfigController'
import BidsController from '../controllers/BidsController'
import BannerController from '../controllers/BannerController'
import middleware from '../middleware/middleware';
/*
user API
*/
router.post('/login',usersControllers.login);
router.post('/register',usersControllers.register);
router.post('/socialLogin',usersControllers.socialLogin);
router.post('/changePassword',middleware.user_api,usersControllers.changePassword);
router.post('/forgotpassword',usersControllers.forgotPassword);
router.get('/me',middleware.user_api,usersControllers.me);
router.post('/update',middleware.user_api,usersControllers.update);
router.get('/logout',middleware.user_api,usersControllers.logout);
router.get('/ranking/:sport_id',middleware.user_api,usersControllers.ranking);

/*
Sport selection Api
*/
router.get('/sports',SportsControllers.getAll);


/*
 League Selection APi
 */

router.get('/leagues',middleware.user_api_conditional,seriesController.leagues);

router.post('/join_league',middleware.user_api,seriesController.joinLeague);
router.post('/leave_league',middleware.user_api,seriesController.leaveLeague);


/*
 Plans Selection APi
 */

router.get('/plans',middleware.user_api,PlansControllers.getAll);

/*
 Config reload APi
 */

router.get('/configurations',ConfigController.getAll);


/*
 Teams Selection APi
 */

router.get('/teams',middleware.user_api,TeamTontroller.getAll);




/*
Purchase Plan API
 */

router.post('/purchase',middleware.user_api,PlansControllers.purchase);




/*
Bid controller API
 */


router.get('/bidStatus/:id',middleware.user_api,BidsController.bidStatus);

router.post('/bid',middleware.user_api,BidsController.bid);
router.delete('/bid',middleware.user_api,BidsController.delete);




/*
Matches Fixtures
 */

router.get('/fixtures',middleware.user_api,MatchController.getByLeagueId);
//router.get('/matches/getLatest',MatchController.getLatest);
//router.get('/fixtures_web',MatchController.getByLeagueId);
router.get('/scores',middleware.user_api,MatchController.getFixture);
//router.get('/banners',BannerController.getAll);


export default router;
