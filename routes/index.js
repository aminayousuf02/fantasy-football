import express from 'express';

const router = express.Router();
import usersControllers from '../controllers/UserControllers'

/* GET index page. */
router.get('/', (req, res) => {
  res.render('index', {
    title: 'Sports'
  });
});
router.get('/logout', (req, res) => {
 delete req.session.user;
 res.redirect('/admin/login')
});
router.get("/activation/:key", usersControllers.activation);
router.get("/changepassword/:key", usersControllers.changepassword);
router.post("/changepassword/:key", usersControllers.updatePassword);


export default router;
