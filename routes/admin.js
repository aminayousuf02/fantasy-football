/**
 * Created by ali on 5/24/18.
 */
import express from 'express';

const router = express.Router();
import middleware from '../middleware/middleware';
import adminController from '../controllers/adminController';
import seriesController from '../controllers/seriesController';
import SportsControllers from '../controllers/SportsControllers';
import TeamController from '../controllers/TeamController';
import MatchController from '../controllers/MatchController';
import ConfigController from '../controllers/ConfigController';
/*
 user API
 */
router.get('/login',adminController.login);
router.get('/',middleware.admin,adminController.index);
router.post('/login',adminController.loginPost);
router.get('/users',middleware.admin,adminController.users);

router.get("/sports",middleware.admin,SportsControllers.getAll)

router.get('/leagues',seriesController.leaguesAdmin);
router.post('/leagues',seriesController.saveLeague);
router.delete('/leagues/:id',seriesController.deleteLeague);
router.put('/leagues/:id',seriesController.update);
router.get('/leagues/:id',seriesController.getOne);

router.get('/league_team',TeamController.getAllBytournamentId);
router.post('/league_teams',TeamController.league_teams);
router.get('/teams',TeamController.getAll);
router.post('/teams',TeamController.saveTeam);
router.delete('/teams/:id',TeamController.delete);


router.get('/matches',MatchController.getByLeagueId);
router.post('/matches',MatchController.saveMatch);
router.delete('/matches/:id',MatchController.delete);
router.put('/matches/:id',MatchController.update);
router.get('/matches/:id',MatchController.getOne);
router.post('/matches_scoreUpdate/:id',MatchController.matches_scoreUpdate);
router.get('/configurations',ConfigController.getAll);
router.post('/configurations',ConfigController.save);


export default router;
