/**
 * Created by ali on 5/24/18.
 */
import userModel from '../models/Users';
var userCache = require("../cache");
const middleware = {
    admin:function(req,res,next){
      if(req.session.user){
          next();
      } else{
          res.redirect('/admin/login')
      }
    },
    user_api:function(req,res,next){
        var token = '';
        console.log('Method = '+req.method)
        console.log('Host = '+req.originalUrl)



        console.log("================================")
        console.log(req.query.auth_token)
        if(req.query.auth_token){
            token =req.query.auth_token;
        }
        if(req.headers.auth_token){
            token =req.headers.auth_token;
        }

        console.log(token)
        if(token && userCache.userToken[token]){

            var userId = userCache.userToken[token];

            req.session.user = userCache.users[userId];
            next()

        }else
        {
            userModel.loginToken(token).then((user)=>{
                 userCache.userToken[user.token] = user.id;
                 userCache.users[user.id] = user;
                req.session.user = user;
                next()

            }).catch(()=> {
                res.status(401);
                res.send({
                    status: false,
                    resCode: 401,
                    message: "Authorization required"
                })
            })
        }

    },
    user_api_conditional:function(req,res,next){
        var token = '';
        console.log('Method = '+req.method)
        console.log('Host = '+req.originalUrl)



        console.log("================================")
        console.log(req.query.auth_token)
        if(req.query.auth_token){
            token =req.query.auth_token;
        }
        if(req.headers.auth_token){
            token =req.headers.auth_token;
        }

        if(token && userCache.userToken[token]){

            var userId = userCache.userToken[token];

            req.session.user = userCache.users[userId];

        }
        next()

    }
}
export default middleware;
