/**
 * Created by ali on 5/22/18.
 */

import mysql from '../config/db';

const Sports = {
    getAll: () => {
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('select id,sport,image from sports', (e, data, fields) => {
                         conn.destroy(); ;
                         if (data)
                             return resolve(data);
                         else
                             return reject()
                     })
                 }
             })
        })
    }

}
export default Sports;
