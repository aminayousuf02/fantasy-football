
import mysql from '../config/db';

const Order = {

    getAll: () => {
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('select s.*,sp.sport from teams as s left join sports as sp on s.sport_id=sp.id', (e, data, fields) => {
                         conn.destroy(); ;
                         if (data)
                              return resolve(data);
                         else
                             return reject()
                     })
                 }
             })
        })


    },
    getOne:(id)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('select s.*,sp.sport from teams as s left join sports as sp on s.sport_id=sp.id where s.id =' + id, (e, data, fields) => {
                         conn.destroy(); ;
                         if (data)
                             return resolve(data[0]);
                         else
                             return reject()
                     })
                 }
             })
        })

    },
    save:(order)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     var fields = [];
                     var value = [];
                     for (var k in order) {
                         fields.push('`' + k + '`')
                         value.push("'" + order[k] + "'");
                     }
                     conn.query('INSERT INTO orders (' + fields.join(',') + ') values (' + value.join(',') + ')', function (error, results, fields) {
                         conn.destroy(); ;

                         if (results)
                             return resolve(results.insertId);
                         else return reject(error);
                     });
                 }
             })
        });
    },
    update:(id,team)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {

                     conn.query('UPDATE teams SET name=?, sport_id= ?, image= ?, country= ?  WHERE id = ?',
                         [team.name, team.sport_id, team.image, team.country, id]
                         , function (error, results, fields) {
                             conn.destroy(); ;

                             if (!error) return resolve(true);
                             else return reject(false);
                         })
                 }
             })
        });
    },
    delete:(id)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {

                     conn.query('delete from teams where id = ' + id, function (error, results, fields) {
                         conn.destroy(); ;

                         if (results) return resolve(true);
                         else return reject(false);
                     });
                 }
             })
        });
    },
}
export default Order;
