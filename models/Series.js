/**
 * Created by ali on 5/24/18.
 */
/**
 * Created by ali on 5/22/18.
 */

import mysql from '../config/db';
import helper from '../Helpers/HelperServices';
const Series = {
    getAll: () => {
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('select s.*,sp.sport from series as s left join sports as sp on s.sport_id=sp.id', (e, data, fields) => {
                         ;
                         if (data) {

                             conn.destroy(); ; return resolve(data);
                         } else
                             return reject()
                     })
                 }
             })
        })
    },
    getAllBySportId: (sport_id) => {
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('select s.*,sp.sport from series as s left join sports as sp on s.sport_id=sp.id where league_status = "active" and  s.sport_id=' + sport_id, (e, data, fields) => {
                         conn.destroy(); ;
                         if (data) {
                             var todate = helper.dateFormat(new Date().getTime());
                             for (var i = 0; i < data.length; i++) {
                                 data[i].isJoined = false;
                                 if (helper.dateFormat(data[i].start_date) == todate) {
                                     data[i].match_start = 'Today';
                                 } else if (data[i].match_status == 'ended') {
                                     data[i].match_start = 'Previous';
                                 } else {
                                     data[i].match_start = 'Upcoming';

                                 }
                             }
                              return resolve(data);
                         }
                         else
                             return reject()
                     })
                 }
             })
        })
    },
    getAll: () => {
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('select s.*,sp.sport from series as s left join sports as sp on s.sport_id=sp.id where league_status = "active"', (e, data, fields) => {
                         conn.destroy(); ;
                         if (data) {
                             var todate = helper.dateFormat(new Date().getTime());
                             for (var i = 0; i < data.length; i++) {
                                 data[i].isJoined = false;
                                 if (helper.dateFormat(data[i].start_date) == todate) {
                                     data[i].match_start = 'Today';
                                 } else if (data[i].match_status == 'ended') {
                                     data[i].match_start = 'Previous';
                                 } else {
                                     data[i].match_start = 'Upcoming';

                                 }
                             }
                              return resolve(data);
                         }
                         else
                             return reject()
                     })
                 }
             })
        })
    },
    getAllBySportIdAndUserId: (sport_id,userId) => {
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {

                     conn.query('select s.*,sp.sport,ul.id as isJoined from series as s left join sports as sp on s.sport_id=sp.id left join userLeagues as ul on (ul.series_id =s.id and ul.user_id=?) where league_status = "active" and s.sport_id=?', [userId, sport_id], (e, data, fields) => {
                         conn.destroy(); ;
                         if (data) {
                             var todate = helper.dateFormat(new Date().getTime());
                             for (var i = 0; i < data.length; i++) {
                                 console.log(data[i])
                                 if (data[i].isJoined) {
                                     data[i].isJoined = true;
                                 } else {
                                     data[i].isJoined = false;
                                 }
                                 if (helper.dateFormat(data[i].start_date) == todate) {

                                     data[i].match_start = 'Today';
                                 } else if (data[i].match_status == 'ended') {
                                     data[i].match_start = 'Previous';
                                 } else {
                                     data[i].match_start = 'Upcoming';

                                 }
                             }
                              return resolve(data);
                         }
                         else
                             return reject(e)
                     })
                 }
             })
        })
    },
    getOne:(id)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('select s.*,sp.sport from series as s left join sports as sp on s.sport_id=sp.id where s.id =' + id, (e, data, fields) => {
                         conn.destroy(); ;
                         if (data)
                              return resolve(data[0]);
                         else
                             return reject()
                     })
                 }
             })
        })

    },

    save:(league)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     var fields = [];
                     var value = [];
                     for (var k in league) {
                         fields.push('`' + k + '`')
                         value.push("'" + league[k] + "'");
                     }
                     conn.query('INSERT INTO series (' + fields.join(',') + ') values (' + value.join(',') + ')', function (error, results, fields) {
                         conn.destroy(); ;
                         if (results)
                              return resolve(results.insertId);
                         else return reject(error);
                     });
                 }
             })
        });
    },
    update:(id,league)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {

                     conn.query('UPDATE series SET title=?, start_date= ?, end_date= ?, league_status= ? , sport_id= ?, image_active= ?, image_inactive= ? WHERE id = ?',
                         [league.title, league.start_date, league.end_date, league.league_status, league.sport_id, league.image_active, league.image_inactive, id]
                         , function (error, results, fields) {
                             conn.destroy(); ;
                             if (!error) return resolve(true);
                             else return reject(false);
                         })
                 }
             })
        });
    },
    joinLeague:(userId,leagueId)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('select id from series where id =?', [leagueId], function (e, r, f) {
                         if (r && r.length == 1) {
                             conn.query('select id from userLeagues where user_id  =? and series_id =?', [userId, leagueId], function (e, c, f) {

                                 if (c && c.length) {
                                     return reject('you have already join this league')
                                 } else {

                                     conn.query('INSERT INTO userLeagues  (`user_id`,`series_id`) values ("' + userId + '","' + leagueId + '")', function (error, results, fields) {
                                         conn.destroy(); ;
                                         if (results)
                                             return resolve(results.insertId);
                                         else return reject(error);
                                     });


                                 }
                             })

                         } else {
                             conn.destroy(); ;
                             return reject('Invalid league id')
                         }
                     })
                 }
             })

        })
    },
    leaveLeague:(userId,leagueId)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('select id from series where id =?', [leagueId], function (e, r, f) {
                         if (r && r.length == 1) {
                             conn.query('select id from userLeagues where user_id  =? and series_id =?', [userId, leagueId], function (e, c, f) {

                                 if (c && c.length) {
                                     conn.query('delete from userLeagues where series_id = ? and user_id = ?', [leagueId, userId], function (error, results, fields) {
                                         conn.destroy(); ;
                                         if (results) return resolve(true);
                                         else return reject(false);
                                     });

                                 } else {
                                     conn.destroy(); ;
                                     return reject('you are not joined this group')
                                 }
                             })

                         } else {
                             return reject('Invalid league id')
                         }
                     })
                 }
             })

        })
    },
    delete:(id)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {

                     conn.query('delete from series where id = ' + id, function (error, results, fields) {
                         conn.destroy(); ;
                         if (results) return resolve(true);
                         else return reject(false);
                     });
                 }
             })
        });
    },
}
export default Series;
