/**
 * Created by ali on 5/22/18.
 */
import mysql from '../config/db';
import helpers from '../Helpers/HelperServices';
const userCache = require("../cache");
var config = require('../cache').config;


console.log("loading model user")
const Users = {
    getAll:new Promise((resolve, reject) => {
        mysql.pool(function (err, conn) {
            if (err) {
                conn.destroy(); ;return reject(err);;
            } else {
                conn.query("select id,name,timezone,image,phone,username,role,email,lastLogin,platform,network,active,balance,coins,points from users where role='user'", (e, data, fields) => {


                    conn.destroy(); ;
                    if(e) reject([])
                    else return resolve(data);
                })
            }
        })

    }),
    userByEmail:(email)=> {
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {                  if (err) {                      conn.destroy(); ;return reject(err);;                  } else {
            conn.query("select id,timezone,name,image,phone,username,email,active,balance,coins,points,password,role from users where email= '" + email + "'", (e, data, fields) => {
                conn.destroy(); ;
                if (data && data.length == 1) {
                     return resolve(data[0]);
                } else {
                    return reject(false)
                }
            })
             }
             })
        })
    },
        loginToken:(token)=>{
        return new Promise((resolve, reject) => {
         mysql.pool(function (err, conn) {
             if (err) {
                 conn.destroy(); ;return reject(err);;
             } else {
                 conn.query("select id,name,image,timezone,phone,username,email,active,balance,coins,points,password,role from users where token= '" + token + "'", (e, data, fields) => {

                     conn.destroy(); ;
                     if (data && data.length == 1) {
                         return resolve(data[0]);
                     } else {
                         return reject(false)
                     }
                 })
             }
         })
    })
    },
    activation:(key)=>{
        return new Promise((resolve, reject) => {
            mysql.pool(function (err, conn) {
                if (err) {
                    conn.destroy(); ;return reject(err);;
                } else {
                    conn.query("select id from users where `keys` = '" + key + "'", (e, data, fields) => {
                        console.log(e)
                        console.log(data)
                        if (data && data.length == 1) {
                            conn.query("update users set active=1 , `keys`='" + new Date().getTime() + "' where `keys`= '" + key + "'", (e, u, fields) => {


                                conn.destroy(); ;
                                if(e) reject(false)
                                else return resolve(data[0]);
                            })
                        } else {
                            conn.destroy(); ;
                            return reject(false)
                        }
                    })
                }
            })

        })
    },
    changePasswordPage:(key)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {                  if (err) {                      conn.destroy(); ;return reject(err);;                  } else {
            console.log(key)
            conn.query("select id from users where `keys` = '"+key+"'",(e,data,fields)=>{
                    console.log(e)
                    console.log(data)
                conn.destroy(); ;
                if(data && data.length==1) {

                         return resolve(data[0]);
                }else{
                    return reject(false)
                }
            })
             }
             })
        })
    },
    forgotPassword:(username)=>{
        return new Promise((resolve, reject) => {
         mysql.pool(function (err, conn) {                  if (err) {                      conn.destroy(); ;return reject(err);;                  } else {
        conn.query("select active,id,name,timezone,image,phone,username,email,active,balance,coins,points,password,role,lastLogin,`keys` from users where  username= '"+username+"' or email = '"+username+"'",(e,data,fields)=>{
            conn.destroy(); ;
            if(data && data.length==1) {

                 return resolve(data[0]);
            }else{
                return reject(e)
            }
        })
         }
         })
    })
    },
    userByUsername:(username)=>{
        return new Promise((resolve, reject) => {
         mysql.pool(function (err, conn) {                  if (err) {                      conn.destroy(); ;return reject(err);;                  } else {
        conn.query("select active,id,timezone,name,image,phone,username,email,active,balance,coins,points,password,role,lastLogin from users where  username= '"+username+"' or email = '"+username+"'",(e,data,fields)=>{

            conn.destroy();

            if(data && data.length==1) {
                 return resolve(data[0]);
            }else{
                return reject(e)
            }
        })
         }
         })
    })
    },
        userById:(id)=>{
        return new Promise((resolve, reject) => {
         mysql.pool(function (err, conn) {
             if (err) {
                 conn.destroy(); ;return reject(err);;
             } else {
                 conn.query("select id,name,image,timezone,phone,username,role,email,active,balance,coins,points,password from users where role='user' and id= '" + id + "'", (e, data, fields) => {
                     conn.destroy(); ;
                     if (data && data.length == 1) {
                          return resolve(data[0]);
                     } else {
                         return reject(e)
                     }
                 })
             }
         })
    })
    },
    loadAllUser:()=>{
         mysql.pool(function (err, conn) {
             if (err) {
                 conn.destroy(); ;return reject(err);;
             } else {
                 conn.query("select id,username,name,phone,timezone,image,email,balance,coins,points,token from users where active=1 and role='user'", (e, data, fields) => {
                     conn.destroy(); ;
                     for (var i = 0; i < data.length; i++) {
                         userCache.userToken[data[i].token] = data[i].id;
                         userCache.users[data[i].id] = data[i];
                     }
                 })
             }
         })

    },
    checkAdmin:()=>{
         mysql.pool(function (err, conn) {
             if (err) {
                 conn.destroy(); ;return reject(err);;
             } else {
                 conn.query("select id from users where role='admin' and email='admin@admin.com'", (e, data, fields) => {
                     if (!data || data.length == 0) {
                         let newUser = {
                             password: helpers.encrypt('123456'.toString()),
                             email: 'admin@admin.com',
                             username: 'Admin',
                             active: 1,
                             role: "admin",
                             keys: helpers.encrypt(new Date().getTime().toString()),
                             points: 0,
                             coins: 0,
                             balance: 0,
                             token: '',
                             lastLogin: new Date().getTime(),
                         };
                         var fields = [];
                         var value = [];
                         for (var k in newUser) {
                             fields.push('`' + k + '`')
                             value.push("'" + newUser[k] + "'");
                         }
                         conn.query('INSERT INTO users (' + fields.join(',') + ') values (' + value.join(',') + ')', function (error, results, fields) {
                             conn.destroy(); ;

                         });


                     }

                 })
             }
         })

    },

    register:(user)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query("select id from users where role='user' and username= '" + user.username + "' or email = '" + user.email + "'", (e, data, fields) => {

                         if (data && data.length > 0) {
                             return reject(data[0]);
                         } else {
                             var fields = [];
                             var value = [];
                             if (!user.name) {
                                 user.name = ''
                             }
                             if (!user.phone) {
                                 user.phone = ''
                             }
                             for (var k in user) {
                                 fields.push('`' + k + '`')
                                 value.push("'" + user[k] + "'");
                             }
                             conn.query('INSERT INTO users (' + fields.join(',') + ') values (' + value.join(',') + ')', function (error, results, fields) {
                                 conn.destroy(); ;
                                 if (results)
                                      return resolve(results.insertId);
                                 else return reject(false);
                             });
                         }
                     });
                 }
             })
    });
    },
    updateDevice:(id,devices,)=>{
         mysql.pool(function (err, conn) {
             if (err) {
                 conn.destroy(); ;return reject(err);;
             } else {
                 if (!devices.timezone) {
                     devices.timezone = '';
                 }
                 if (!devices.country) {
                     devices.country = '';
                 }
                 if (!devices.ip) {
                     devices.ip = '';
                 }
                 if (!devices.coins) {
                     devices.coins = 0;
                 }
                 conn.query('UPDATE users SET coins=?, timezone=?, country=?, ip=?, token=?, deviceId = ?, device_token= ?, platform = ? , lastLogin = ? WHERE id = ?',
                     [devices.coins, devices.timezone, devices.country, devices.ip, devices.token, devices.deviceId, devices.device_token, devices.platform, new Date().getTime(), id], function (error, results, fields) {
                         conn.destroy(); ;
                     })
             }
         })
    },
    changePassword:(id,password)=>{
        return new Promise((resolve,reject)=>{
         mysql.pool(function (err, conn) {
             if (err) {
                 conn.destroy(); ;return reject(err);;
             } else {
                 conn.query('UPDATE users SET password=? WHERE id = ?', [password, id], function (error, results, fields) {
                     conn.destroy(); ;
                     if (!error) return resolve(true)
                     return reject(true)
                 })
             }
         })
        })
    },
    changePasswordWithKey:(id,password)=>{
        return new Promise((resolve,reject)=>{
         mysql.pool(function (err, conn) {
             if (err) {
                 conn.destroy(); ;return reject(err);;
             } else {
                 conn.query('UPDATE users SET password=? , `keys`="' + new Date().getTime() + '" WHERE `keys` = ?', [password, id], function (error, results, fields) {
                     conn.destroy(); ;
                     if (!error) return resolve(true)
                     return reject(true)
                 })
             }
         })
        })
    },
    updateToken:(id,devices)=>{
         mysql.pool(function (err, conn) {
             if (err) {
                 conn.destroy(); ;return reject(err);;
             } else {
                 conn.query('UPDATE users SET token=?, lastLogin = ? WHERE id = ?', [devices.token, new Date().getTime(), id], function (error, results, fields) {
                     conn.destroy(); ;
                 })
             }
         })
    },
    updateTokenLogin:(id,devices)=>{

         mysql.pool(function (err, conn) {
             if (err) {
                 conn.destroy(); ;return reject(err);;
             } else {
                 if (!devices.timezone) {
                     devices.timezone = '';
                 }
                 if (!devices.country) {
                     devices.country = '';
                 }
                 if (!devices.ip) {
                     devices.ip = '';
                 }

                 if (!devices.coins) {
                     devices.coins = 0;
                 }

                 conn.query('UPDATE users SET  coins=?,  timezone=?, country=?, ip=?, token=?, lastLogin = ? WHERE id = ?', [devices.coins, devices.timezone, devices.country, devices.ip, devices.token, new Date().getTime(), id], function (error, results, fields) {
                     conn.destroy(); ;
                 })
             }
         })
    },
    socialLogin:(user,host)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query("select lastLogin,id,role,username,timezone,email,active,balance,coins,points,token from users where role='user' and (email= '" + user.email + "' or (network='" + user.network + "' and networkId='" + user.networkId + "'))", (e, data, fields) => {
                         if (data && data.length > 0) {

                             data = data[0];
                             if (!user.timezone) {
                                 user.timezone = '';
                             }
                             if (!user.country) {
                                 user.country = '';
                             }
                             if (!user.ip) {
                                 user.ip = '';
                             }
                             if (!data.coins) {
                                 data.coins = 0;
                             }

                             var todate = helpers.dateFormat(new Date().getTime());
                             var userLogin = helpers.dateFormat(data.lastLogin);

                             var coins = parseInt(data.coins);
                             console.log(userLogin + " == " + todate)
                             if (userLogin != todate) {
                                 if (config.dailyLogin && config.dailyLogin > 0) {
                                     user.coins = coins + parseInt(config.dailyLogin);
                                 }
                             }else{
                                 user.coins = coins ;
                             }


                             conn.query('UPDATE users SET coins=?, timezone=?, country=?, ip=?,network=?,networkId=?,token=?, deviceId = ?, device_token= ?, platform = ? , lastLogin = ? WHERE id = ?',
                                 [user.coins, user.timezone, user.country, user.ip, user.network, user.networkId, user.token, user.deviceId, user.device_token, user.platform, new Date().getTime(), data.id], function (error, results, fields) {


                                     userCache.userToken[user.token] = data.id;
                                     userCache.users[data.id] = user;
                                     conn.destroy(); ;
                                     if(error) return reject(error);
                                     else return resolve(user);

                                 })


                         }
                         else if (!e) {
                             var fields = [];
                             var value = [];
                             if (!user.name) {
                                 user.name = ''
                             }
                             if (!user.phone) {
                                 user.phone = ''
                             }
                             if (config.onRegister) {
                                 user.coins = config.onRegister;
                             }

                             var password = user.password;
                             user.password = helpers.encrypt(user.password)
                             for (var k in user) {
                                 fields.push('`' + k + '`')
                                 value.push("'" + user[k] + "'");
                             }
                             conn.query('INSERT INTO users (' + fields.join(',') + ') values (' + value.join(',') + ')', function (error, results, fields) {

                                 user.id = results.insertId;
                                 userCache.userToken[user.token] = user.id;
                                 helpers.socialLogin(user,host,password)
                                 userCache.users[user.id] = user;

                                 conn.destroy(); ;
                                 if(error) return reject(error);
                                 else return resolve(user);

                             });
                         } else {
                             conn.destroy(); ;
                             return reject(false)
                         }
                     });
                 }
             })

    });
    },
    updateCoins:(coins,id)=>{
        return new Promise((resolve, reject) => {

             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('UPDATE users SET coins=?, lastLogin = ? WHERE id = ?', [coins, new Date().getTime(), id], function (error, results, fields) {
                         conn.destroy(); ;
                         if (error) {
                             return reject();
                         } else {
//`userCache.userToken[data[i].token]=data[i].id;
                             userCache.users[id].coins = coins;

                             return resolve();
                         }

                     })
                 }
             })
            })

    },
    update:(user,id)=>{
        return new Promise((resolve, reject) => {

             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('UPDATE users SET name=?, image= ?,phone=? WHERE id = ?', [user.name, user.image, user.phone, id], function (error, results, fields) {
                         conn.destroy(); ;
                         if (error) {
                             return reject(error);
                         } else {
                              return resolve();
                         }

                     })

                 }
             })
            })

    }

}
console.log("completed model user")

export default Users;
