/**
 * Created by ali on 10/7/18.
 */

import mysql from '../config/db';
var config = require('../cache').config;
import helper from '../Helpers/HelperServices';

const Banner = {
    getAll: (seriesId) => {
        return new Promise((resolve, reject) => {
            mysql.pool(function (err, conn) {
                if (err) {
                    conn.destroy(); ;return reject(err);;
                } else {
                    let query = 'select * from banners where active=1';
                    if (seriesId ) query ="select * from series where id = "+seriesId;
                    conn.query(query, (e, data, fields) => {
                        conn.destroy(); ;
                        if (data) {
                            if (seriesId && data && data.length > 0) {
                                data[0].start_date = helper.dateFormat(data[0].start_date)
                                data[0].end_date = helper.dateFormat(data[0].end_date)
                            }
                            return resolve(data);
                        }
                        else
                            return reject()
                    })
                }
            })
        })
    }

}
export default Banner;
