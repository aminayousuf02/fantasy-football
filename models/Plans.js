/**
 * Created by ali on 5/27/18.
 */
/**
 * Created by ali on 5/24/18.
 */
/**
 * Created by ali on 5/22/18.
 */

import mysql from '../config/db';
import helper from '../Helpers/HelperServices';
const Plans = {
    getAll: () => {
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('select `id`, `coins`, `price`, `currency` from plans', (e, data, fields) => {
                         conn.destroy(); ;
                         if (data) {
                              return resolve(data);

                         } else
                             return reject()
                     })
                 }
             })
        })
    },
    getOne:(id)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('select * from plans where id =' + id, (e, data, fields) => {
                         conn.destroy(); ;
                         if (data)
                              return resolve(data[0]);
                         else
                             return reject(e)
                     })
                 }
             })
        })

    },
    save:(league)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     var fields = [];
                     var value = [];
                     for (var k in league) {
                         fields.push('`' + k + '`')
                         value.push("'" + league[k] + "'");
                     }
                     conn.query('INSERT INTO plans (' + fields.join(',') + ') values (' + value.join(',') + ')', function (error, results, fields) {
                         conn.destroy(); ;
                         if (results)
                              return resolve(results.insertId);
                         else return reject(false);
                     });
                 }
             })
        });
    },
    update:(id,match)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('UPDATE matches SET coins=?, price=?, currency= ? WHERE id = ?',
                         [match.coins, match.price, match.currency, id]
                         , function (error, results, fields) {
                             conn.destroy(); ;
                             if (!error)return resolve(true);
                             else return reject(error);
                         })
                 }
             })
        });
    },
    delete:(id)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {

                     conn.query('delete from plans where id = ' + id, function (error, results, fields) {
                         conn.destroy(); ;
                         if (results) return resolve(true);
                         else return reject(false);
                     });
                 }
             })
        });
    },
}
export default Plans;
