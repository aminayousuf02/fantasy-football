/**
 * Created by ali on 5/27/18.
 */
/**
 * Created by ali on 5/24/18.
 */
/**
 * Created by ali on 5/22/18.
 */
var moment = require('moment-timezone');
import mysql from '../config/db';
import helper from '../Helpers/HelperServices';
var config = require('../cache').config;
import UserModel from './Users'
const Series = {
    getAll: () => {
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {

                     conn.query('select m.*,t1.name as team_a,t1.image as team_a_flag,t2.name as team_b,t2.image as team_b_flag from matches as m' +
                         ' inner join teams as t1 on t1.id = m.team_a_id' +
                         ' inner join teams as t2 on t2.id = m.team_b_id' +
                         '', (e, data, fields) => {
                         conn.destroy(); ;

                         if (data) {

                             var todate = helper.dateFormat(new Date().getTime());
                             console.log("todate is ");
                             console.log(todate);
                             for (var i = 0; i < data.length; i++) {
                                 if (helper.dateFormat(data[i].start_date) == todate) {
                                     data[i].match_start = 'Today';
                                     console.log("if works")
                                 } else if (data[i].match_status == 'ended') {
                                     data[i].match_start = 'Previous';console.log("else if works")
                                 } else {
                                     data[i].match_start = 'Upcoming';
                                     console.log("else works")

                                 }
                             }
                              return resolve(data);

                         } else
                             return reject()
                     })
                 }
             })
        })
    },
    getByLeagueId: (series_id,user) => {
        var userId = user.id;
        console.log(user)
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {

                     conn.query('select b.id as bidId, m.*,t1.name as team_a,t1.image as team_a_flag,t2.name as team_b,t2.image as team_b_flag from matches as m' +
                         ' inner join teams as t1 on t1.id = m.team_a_id' +
                         ' left join bids as b on b.match_id = m.id and b.user_id= ' + userId + ' ' +
                         ' inner join teams as t2 on t2.id = m.team_b_id' +
                         ' where m.series_id = ' + series_id+" order by  m.start_date desc", (e, data, fields) => {
                         ;
                         conn.destroy(); ;
                         if (data) {
                             var todate = helper.dateFormat(new Date().getTime());
                             for (var i = 0; i < data.length; i++) {
                                 moment.tz.setDefault(data[i].timezone);

                                 var a = moment.tz(helper.dateFormat(data[i].start_date) + " " + helper.MINHR(data[i].start_time), data[i].timezone);
                                 var b = moment.tz(a.format(), user.timezone);

                                 var localTime = new Date(b.format());

                                 var endDate = new Date();
                                 endDate.setDate(endDate.getDate()-1)
                                 endDate.setHours(23)
                                 endDate.setMinutes(59)
                                 endDate.setSeconds(59)
                                 data[i].local_time = b.format('hh:mm A');
                                 data[i].local_timestamp = b.format();
                                 data[i].timestamp = a.format();
                                 data[i].isBid = false;
                                 if (data[i].bidId != null) {
                                     data[i].isBid = true;
                                 }
                                 delete data[i].bidId;
                                 console.log(helper.dateFormat(data[i].start_date)+" = "+todate)
                                 if (helper.dateFormat(data[i].start_date) == todate) {
                                     data[i].match_start = 'Today';
                                 } else if (new Date(helper.dateFormat(data[i].start_date)+" 00:00:00").getTime() < endDate) {
                                     data[i].match_start = 'Previous';
                                 } else {
                                     data[i].match_start = 'Upcoming';

                                 }
                                 data[i].start_date = helper.dateMonthYear(data[i].start_date);
                                 data[i].start_time = helper.minToHr(data[i].start_time);
                             }

                             return resolve(data);

                         } else
                             return reject()
                     })
                 }
             })
        })
    },
    getAllBySportId: (sport_id) => {
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('select s.*,sp.sport from series as s left join sports as sp on s.sport_id=sp.id where s.sport_id=' + sport_id, (e, data, fields) => {
                         conn.destroy(); ;
                         if (data) {

                             var todate = helper.dateFormat(new Date().getTime());
                             for (var i = 0; i < data.length; i++) {
                                 if (helper.dateFormat(data[i].start_date) == todate) {
                                     data[i].match_start = 'Today';
                                 } else if (data[i].match_status == 'ended') {
                                     data[i].match_start = 'Previous';
                                 } else {
                                     data[i].match_start = 'Upcoming';

                                 }
                             }
                            return resolve(data);
                         } else
                             return reject()
                     })
                 }
             })
        })
    },
    getOne:(id)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('select * from matches where id =' + id, (e, data, fields) => {
                         conn.destroy(); ;
                         if (data && data[0])
                              return resolve(data[0]);
                         else
                             return reject()
                     })
                 }
             })
        })

    },
    getFixture: (user,leagueId) => {
        console.log(user.timezone)
        var user_id = user.id;
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                    var query = 'select m.*,t1.name as team_a,b.team_id as bid_team_id,b.bid_status as bid_status,t1.image as team_a_flag,t2.name as team_b,t2.image as team_b_flag from matches as m' +
                        ' inner join teams as t1 on t1.id = m.team_a_id' +
                        ' inner join teams as t2 on t2.id = m.team_b_id' +
                        ' inner join bids as b on b.match_id = m.id ' +
                        ' where b.user_id = ' + user_id+"  order by m.start_date desc";
                     if(leagueId) {
                         query = 'select m.*,t1.name as team_a,b.team_id as bid_team_id,b.bid_status as bid_status,t1.image as team_a_flag,t2.name as team_b,t2.image as team_b_flag from matches as m' +
                             ' inner join teams as t1 on t1.id = m.team_a_id' +
                             ' inner join teams as t2 on t2.id = m.team_b_id' +
                             ' inner join bids as b on b.match_id = m.id ' +
                             ' where b.user_id = ' + user_id+" and m.series_id="+leagueId+"  order by m.start_date desc";

                     }
                    console.log(query)

                     conn.query(query, (e, data, fields) => {

                         conn.destroy(); ;
                         if (data) {
                             var todate = helper.dateFormat(new Date().getTime());
                             for (var i = 0; i < data.length; i++) {
                                 var a = moment.tz(helper.dateFormat(data[i].start_date) + " " + helper.MINHR(data[i].start_time), data[i].timezone);
                                 var b = moment.tz(helper.dateFormat(data[i].start_date) + " " + helper.MINHR(data[i].start_time), user.timezone);

                                 var a = moment.tz(helper.dateFormat(data[i].start_date) + " " + helper.MINHR(data[i].start_time), data[i].timezone);
                                 var b = moment.tz(a.format(), user.timezone);

                                 console.log(a.format());
                                 console.log(b.format());
                                 var localTime = new Date(b.format());

                                 data[i].local_time = b.format('hh:mm A');
                                 data[i].local_timestamp = b.format();
                                 data[i].timestamp = a.format();

                                 if (data[i].bid_team_id == data[i].team_b_id) {
                                     data[i].bid_team_name = data[i].team_b;
                                     data[i].bid_team_flag = data[i].team_b_flag;
                                 }
                                 if (data[i].bid_team_id == data[i].team_a_id) {
                                     data[i].bid_team_name = data[i].team_a;
                                     data[i].bid_team_flag = data[i].team_a;
                                 }
                                 var endDate = new Date();
                                 endDate.setDate(endDate.getDate()-1)
                                 endDate.setHours(23)
                                 endDate.setMinutes(59)
                                 endDate.setSeconds(59)
                                 if (helper.dateFormat(data[i].start_date) == todate) {
                                     data[i].match_start = 'Today';
                                 } else if (new Date(helper.dateFormat(data[i].start_date)+" 00:00:00").getTime() < endDate) {
                                     data[i].match_start = 'Previous';
                                 } else {
                                     data[i].match_start = 'Upcoming';

                                 }

                                 data[i].start_date = helper.dateMonthYear(data[i].start_date);
                                 data[i].start_time = helper.minToHr(data[i].start_time);
                             }


                             return resolve(data);

                         } else
                             return reject()
                     })
                 }
             })
        })
    },
    save:(league)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     var fields = [];
                     var value = [];
                     for (var k in league) {
                         fields.push('`' + k + '`')
                         value.push("'" + league[k] + "'");
                     }
                     console.log(fields)
                     conn.query('INSERT INTO matches (' + fields.join(',') + ') values (' + value.join(',') + ')', function (error, results, fields) {
                         conn.destroy(); ;
                         if (results)
                              return resolve(results.insertId);
                         else return reject(false);
                     });
                 }
             })
        });
    },
    update:(id,match)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     console.log(match.stronger_team_id + "  = stronger_team_id")
                     conn.query('UPDATE matches SET audio_link=?,video_link=?,team_a_id=?, team_b_id=?, start_date= ?, end_date= ?, sport_id= ? ,' +
                         ' series_id= ?, venue= ?, group_info= ?,start_time=?,timezone=?, stronger_team_id = ? WHERE id = ?',
                         [match.audio_link,match.video_link, match.team_a_id, match.team_b_id, match.start_date, match.end_date, match.sport_id, match.series_id, match.venue,
                             match.group_info, match.start_time, match.timezone, match.stronger_team_id, id]
                         , function (error, results, fields) {
                             conn.destroy(); ;
                             if (!error)return resolve(true);
                             else return reject(error);
                         })
                 }
             })
        });
    },
    scoreUpdate:(id,match)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('UPDATE matches SET team_a_score=?, team_b_score=?, isLive= ?, team_win_id= ?, match_status= ? ,' +
                         ' result= ? WHERE id = ?',
                         [match.team_a_score, match.team_b_score, match.isLive, match.team_win_id, match.match_status, match.result, id]
                         , function (error, results, fields) {
                             conn.destroy(); ;
                             if (!error)return resolve(true);
                             else return reject(error);
                         })
                 }
             })
        });
    },
    delete:(id)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {

                     conn.query('delete from matches where id = ' + id, function (error, results, fields) {
                         conn.destroy(); ;
                         if (results) return resolve(true);
                         else return reject(false);
                     });
                 }
             })
        });
    },
    updateCoins:function(id,match,cb){
         mysql.pool(function (err, conn) {
             if (err) {
                 conn.destroy(); ;return reject(err);;
             } else {

                 if (match.match_status == 'ended') {
                     Series.distributeCoins(id, match, function () {
                         conn.destroy(); ;
                         cb()
                     });
                 } else {
                     conn.query('select * from matches where id =' + id, (e, data, fields) => {
                         conn.destroy(); ;
                         if (data && data[0]) {
                             data = data[0];
                             if (data.match_status == 'ended') {
                                 Series.revertCoins(id, function () {
                                     cb()
                                 });
                             } else {
                                 cb()
                             }
                         } else {
                             cb()
                         }
                     })
                 }
             }
         })


    },
    distributeCoins:function(match_id,match,cb){
         mysql.pool(function (err, conn) {
             if (err) {
                 conn.destroy(); ;return reject(err);;
             } else {
                 conn.query('select u.id,u.coins,b.coins as bidCoins,b.team_id from users as u inner join bids as b on b.user_id=u.id where b.coins = 0 and b.match_id=' + match_id, (e, data, fields) => {
                     if (e) {
                         console.log(e)
                     } else {
                         var roi = 200;
                         var roiweak = 200;
                         if (match.team_win_id == match.stronger_team_id) {
                             if (config.roistrong) {
                                 roi = parseInt(config.roistrong)
                             }
                         } else {
                             if (config.roiweak) {
                                 roi = parseInt(config.roiweak)
                                 roiweak = parseInt(config.roiweak)
                             }
                         }

                         roi = (roi / 100) * parseInt(config.onBid);
                         roiweak = (roiweak / 100) * parseInt(config.onBid);

                         conn.query('update  bids set coins = ' + roi + ',bid_status="Won" where team_id="' + match.team_win_id + '" and match_id=' + match_id, (e, d, fields) => {
                             conn.query('update  bids set coins = ' + roiweak + ',bid_status="Lost" where  team_id!="' + match.team_win_id + '" and match_id=' + match_id, (e, d, fields) => {
                                 conn.destroy(); ;
                                 for (var i = 0; i < data.length; i++) {
                                     if (match.team_win_id == data[i].team_id) {
                                         if (!data[i].coins || data[i].coins == '') {
                                             data[i].coins = 0;
                                         }
                                         var coins = parseInt(data[i].coins) + parseInt(roi)
                                         console.log("Coins = " + coins)
                                         console.log("userId = " + data[i].id)
                                         console.log(coins)
                                         UserModel.updateCoins(coins, data[i].id).catch(() => {
                                         });
                                     } else {

                                         if (!data[i].coins || data[i].coins == '') {
                                             data[i].coins = 0;
                                         }
                                         var coins = parseInt(data[i].coins) - parseInt(roiweak) - parseInt(config.onBid)
                                         UserModel.updateCoins(coins, data[i].id).catch(() => {
                                         });

                                     }
                                 }
                                 cb()
                             })
                         })
                     }
                 })
             }
         })
    },
    revertCoins:function(match_id,cb){
         mysql.pool(function (err, conn) {
             if (err) {
                 conn.destroy(); ;return reject(err);;
             } else {
                 conn.query('select u.id,u.coins,b.coins as bidCoins,b.bidStatus from users as u inner join bids as b on b.user_id=u.id where b.match_id=' + match_id, (e, data, fields) => {
                     conn.query('update  bids set coins = 0,bid_status="Pending" where match_id=' + match_id, (e, d, fields) => {
                         conn.destroy(); ;
                         for (var i = 0; i < data.length; i++) {
                             if (data[i].bidStatus == 'Won') {
                                 var coins = parseInt(data[i].coins) - parseInt(data[i].bidCoins)
                                 UserModel.updateCoins(coins, data[i].id).catch(() => {
                                 });
                             } else if (data[i].bidStatus == 'Lost') {
                                 var coins = parseInt(data[i].coins) + parseInt(data[i].bidCoins)
                                 UserModel.updateCoins(coins, data[i].id).catch(() => {
                                 });
                             }
                         }
                         cb();
                     })
                 })
             }
         })
    }
}
export default Series;
