
/**
 * Created by ali on 5/22/18.
 */

import mysql from '../config/db';
import helper from '../Helpers/HelperServices';
var configCache = require("../cache");
const Config = {
    getAll: () => {
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('select * from config', (e, data, fields) => {
                         ;
                         if (data) {
                             for (var i = 0; i < data.length; i++) {
                                 configCache.config[data[i].param] = data[i].value;
                             }
                             conn.destroy(); ; return resolve(configCache.config);

                         } else {
                             conn.destroy(); ;
                             return reject()
                         }
                     })
                 }
             })
        })
    },
}
export default Config;
