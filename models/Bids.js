/**
 * Created by ali on 5/22/18.
 */

import mysql from '../config/db';
import helper from '../Helpers/HelperServices';
import userModel from './Users';
var config = require('../cache').config;
const Bids = {
    bidStatus: (id) => {
        return new Promise((resolve, reject) => {
            mysql.pool(function (err, conn) {
                if (err) {
                    conn.destroy(); ;return reject(err);;
                } else {
                    conn.query('SELECT count(bids.team_id) as bid_count,bids.team_id,teams.name,teams.image,matches.stronger_team_id FROM `bids` ' +
                        ' inner join matches on bids.match_id= matches.id ' +
                        'inner join teams on teams.id=bids.team_id where match_id=? group by team_id', [id], (e, data, fields) => {
                        if (data) {

                            for (var i = 0; i < data.length; i++) {
                                data[i].roi = config['roistrong'];

                                if (data[i].team_id == data[i].stronger_team_id) {
                                    data[i].isStronger = true;
                                    data[i].roi = config['roistrong'];
                                } else if (data[i].stronger_team_id != 0) {
                                    data[i].isStronger = false;
                                    data[i].roi = config['roiweak'];
                                }
                            }
                            if (data.length == 2) {
                                conn.destroy(); ; return resolve(data);
                            } else {
                                conn.query('SELECT  count(bids.team_id) as bid_count,bids.team_id,' +
                                    ' matches.team_a_id , a.name as team_a,a.image as team_a_image,' +
                                    ' matches.team_b_id , b.name as team_b,b.image as team_b_image,' +
                                    'matches.stronger_team_id ' +
                                    ' FROM `matches` ' +
                                    ' left join bids on bids.match_id= matches.id ' +
                                    ' left join teams as a on a.id=matches.team_a_id ' +
                                    ' left join teams as b on b.id=matches.team_b_id ' +
                                    'where match_id=? ', [id], (e, data, fields) => {

                                    ;
                                    if (e){
                                        conn.destroy(); ;
                                        reject(e)
                                    }
                                    else {

                                        var d = [];
                                        var isStronger = false;
                                        var roi = config['roiweak'];
                                        if (data[0].stronger_team_id == data[0].team_a_id) {
                                            isStronger = true;
                                            roi = config['roistrong'];
                                        }
                                        if (data[0].stronger_team_id == 0) roi = config['roistrong'];
                                        var bidCount = 0;
                                        if (data[0].team_id == data[0].team_a_id) bidCount = data[0].bid_count;

                                        d.push({
                                            "bid_count": bidCount,
                                            "team_id": data[0].team_a_id,
                                            "name": data[0].team_a,
                                            "image": data[0].team_a_image,
                                            "stronger_team_id": data[0].stronger_team_id,
                                            "roi": roi,
                                            "isStronger": isStronger
                                        })

                                        var isStronger = false;
                                        var roi = config['roiweak'];
                                        if (data[0].stronger_team_id == data[0].team_b_id) {
                                            isStronger = true;
                                            roi = config['roistrong'];
                                        }
                                        var bidCount = 0;
                                        if (data[0].team_id == data[0].team_b_id) bidCount = data[0].bid_count;
                                        if (data[0].stronger_team_id == 0) roi = config['roistrong'];

                                        d.push({
                                            "bid_count": bidCount,
                                            "team_id": data[0].team_b_id,
                                            "name": data[0].team_b,
                                            "image": data[0].team_b_image,
                                            "stronger_team_id": data[0].stronger_team_id,
                                            "roi": roi,
                                            "isStronger": isStronger
                                        })
                                        conn.destroy(); ; return resolve(d);
                                    }
                                })
                            }

                        } else {
                            conn.destroy(); ;
                            return reject()
                        }
                    })


                }

            })
        })
    },
    binOnMatch:(matchId,user,teamId)=>{
        return new Promise((resolve, reject) => {
            mysql.pool(function (err, conn) {                  if (err) {                      conn.destroy(); ;return reject(err);;                  } else {
            var userId = user.id;
                conn.query('select * from bids where user_id =? and  match_id =? ', [userId, matchId], function (error, exists, fields) {
                conn.query('delete from bids where user_id =? and  match_id =? ', [userId, matchId], function (error, results, fields) {



                    if(exists.length==0){
                        var coins = 0;
                        if(config.onBid) coins = config.onBid;
                        user.coins -= coins;
                        if(user.coins>=0) {
                            userModel.updateCoins(user.coins, userId).then(() => {
                            }).catch(() => {
                            })
                        }else{
                            conn.destroy(); ;
                            return reject(', insufficient coins ');
                        }
                    }

                    var bids = {
                        team_id:teamId,
                        match_id:matchId,
                        user_id:userId
                    }
                    var fields = [];
                    var value = [];
                    for(var k in bids){
                        fields.push('`'+k+'`')
                        value.push("'"+bids[k]+"'");
                    }
                    conn.query('INSERT INTO bids ('+fields.join(',')+') values ('+value.join(',')+')', function (error, results, fields) {
                         ;
                        if(results) {
                            conn.destroy(); ;
                            return resolve(user);
                        }
                        else {
                            conn.destroy(); ;
                            return reject(error);
                        }
                    });
            })
            })
                }
            })
        })
    },
    ranking:(sport_id)=>{
        return new Promise((resolve, reject) => {
                 mysql.pool(function (err, conn) {
                     if (err) {
                         conn.destroy(); ;return reject(err);;

                     } else {
/*

                         SELECT (select count(bids.id) from bids where user_id=u.id and bid_status="Pending" and t1.id=b1.team_id and t1.sport_id=2) as bid_pending, (select count(id) from bids where user_id=u.id and bid_status="Won" and t2.id=b2.team_id and t2.sport_id=2) as bid_win, (select count(id) from bids where user_id=u.id and bid_status="Lost" and t3.id=b3.team_id and t3.sport_id=2) as bid_lost, u.username, u.id FROM users as u
                         inner join bids as b1 on b1.user_id=u.id
                         inner join bids as b2 on b2.user_id=u.id
                         inner join bids as b3 on b2.user_id=u.id
                         inner join teams as t1 on b1.team_id=t1.id
                         inner join teams as t2 on b2.team_id=t2.id
                         inner join teams as t3 on b3.team_id=t3.id
                         group by u.id*/

var query1 = 'SELECT count(b1.id)  as bid_pending,  u.username, u.id FROM users as u ' +
    'inner join bids as b1 on b1.user_id=u.id ' +
    'inner join teams as t1 on b1.team_id=t1.id ' +
    'where b1.bid_status = "Pending" ' +
    ' and t1.sport_id='+sport_id+' '+
    ' group by u.id';
 var query2 = 'SELECT count(b1.id)  as bid_won,  u.username, u.id FROM users as u ' +
    'inner join bids as b1 on b1.user_id=u.id ' +
    'inner join teams as t1 on b1.team_id=t1.id ' +
    'where b1.bid_status = "Won" ' +
    ' and t1.sport_id='+sport_id+' '+
    ' group by u.id';
 var query3 = 'SELECT count(b1.id)  as bid_lost,  u.username, u.id FROM users as u ' +
    'inner join bids as b1 on b1.user_id=u.id ' +
    'inner join teams as t1 on b1.team_id=t1.id ' +
    'where b1.bid_status = "Lost"' +
    ' and t1.sport_id='+sport_id+' '+
    'group by u.id';



 conn.query(query1,  (e, data1, fields) => {
console.log(e)
     console.log(data1)
     conn.query(query2, (e, data2, fields) => {
         conn.query(query3, (e, data3, fields) => {


             var data = [];
             var dataObj = {};

             for (var i = 0; i < data1.length; i++) {

                 dataObj[data1[i].id] = {
                     bid_pending: data1[i].id?data1[i].bid_pending:0,
                     bid_lost:0,
                     bid_won:0,
                     username:data1[i].username,
                     id:data1[i].id,
                 }

             }


             for (var i = 0; i < data2.length; i++) {

                if(!dataObj[data2[i].id] ) dataObj[data2[i].id]={}
                if(!dataObj[data2[i].id].bid_pending) dataObj[data2[i].id].bid_pending=0

                 dataObj[data2[i].id] = {
                     bid_pending:dataObj[data2[i].id].bid_pending,
                     bid_lost:0,
                     bid_won:data2[i].bid_won,
                     username:data2[i].username,
                     id:data2[i].id,
                 }

             }


             for (var i = 0; i < data3.length; i++) {
                 if(!dataObj[data3[i].id] ) dataObj[data3[i].id]={}

                 if( !dataObj[data3[i].id].bid_pending) dataObj[data3[i].id].bid_pending=0
                 if( !dataObj[data3[i].id].bid_won) dataObj[data3[i].id].bid_won=0

                 dataObj[data3[i].id] = {
                     bid_pending: dataObj[data3[i].id].bid_pending,
                     bid_lost:data3[i].bid_lost,
                     bid_won:dataObj[data3[i].id].bid_won,
                     username:data3[i].username,
                     id:data3[i].id,
                 }

             }
             for(var k in dataObj){
             data.push(dataObj[k])
            }


             if (e) {
                 conn.destroy();
                 ;
                 return reject(e);
             }
             else {
                 conn.destroy();
                 ;
                 resolve(data)
             }
         })
     })
 })
                     }

            })
        })
    },
    rankingByLeagueId:(sport_id,league_id)=>{
        return new Promise((resolve, reject) => {
                 mysql.pool(function (err, conn) {
                     if (err) {
                         conn.destroy(); ;return reject(err);;

                     } else {
/*

                         SELECT (select count(bids.id) from bids where user_id=u.id and bid_status="Pending" and t1.id=b1.team_id and t1.sport_id=2) as bid_pending, (select count(id) from bids where user_id=u.id and bid_status="Won" and t2.id=b2.team_id and t2.sport_id=2) as bid_win, (select count(id) from bids where user_id=u.id and bid_status="Lost" and t3.id=b3.team_id and t3.sport_id=2) as bid_lost, u.username, u.id FROM users as u
                         inner join bids as b1 on b1.user_id=u.id
                         inner join bids as b2 on b2.user_id=u.id
                         inner join bids as b3 on b2.user_id=u.id
                         inner join teams as t1 on b1.team_id=t1.id
                         inner join teams as t2 on b2.team_id=t2.id
                         inner join teams as t3 on b3.team_id=t3.id
                         group by u.id*/

var query1 = 'SELECT count(b1.id)  as bid_pending,  u.username, u.id FROM users as u ' +
    ' inner join bids as b1 on b1.user_id=u.id ' +
    ' inner join teams as t1 on b1.team_id=t1.id ' +
    ' inner join matches as m1 on m1.id= b1.match_id'+
    ' where b1.bid_status = "Pending" ' +
    ' and t1.sport_id='+sport_id+' '+
    ' and m1.series_id='+league_id+' '+
    ' group by u.id';
 var query2 = 'SELECT count(b1.id)  as bid_won,  u.username, u.id FROM users as u ' +
    ' inner join bids as b1 on b1.user_id=u.id ' +
    ' inner join teams as t1 on b1.team_id=t1.id ' +
     ' inner join matches as m1 on m1.id= b1.match_id'+
     ' where b1.bid_status = "Won" ' +
    ' and t1.sport_id='+sport_id+' '+
     ' and m1.series_id='+league_id+' '+
     ' group by u.id';
 var query3 = 'SELECT count(b1.id)  as bid_lost,  u.username, u.id FROM users as u ' +
    ' inner join bids as b1 on b1.user_id=u.id ' +
    ' inner join teams as t1 on b1.team_id=t1.id ' +
     ' inner join matches as m1 on m1.id= b1.match_id'+
     ' where b1.bid_status = "Lost" ' +
    ' and t1.sport_id='+sport_id+' '+
     ' and m1.series_id='+league_id+' '+
     ' group by u.id';



 conn.query(query1,  (e, data1, fields) => {
console.log(e)
     console.log(data1)
     conn.query(query2, (e, data2, fields) => {
         conn.query(query3, (e, data3, fields) => {

             console.log(query2)
             console.log(data2)
             if(!data2){
                 data2 =[];
             }

             if(!data3){
                 data3 =[];
             }

             var data = [];
             var dataObj = {};

             for (var i = 0; i < data1.length; i++) {

                 dataObj[data1[i].id] = {
                     bid_pending: data1[i].id?data1[i].bid_pending:0,
                     bid_lost:0,
                     bid_won:0,
                     username:data1[i].username,
                     id:data1[i].id,
                     rank:0,
                 }

             }


             for (var i = 0; i < data2.length; i++) {
console.log(data2[i])

                if(!dataObj[data2[i].id] ) dataObj[data2[i].id]={}
                if(!dataObj[data2[i].id].bid_pending) dataObj[data2[i].id].bid_pending=0

                 dataObj[data2[i].id] = {
                     bid_pending:dataObj[data2[i].id].bid_pending,
                     bid_lost:0,
                     bid_won:data2[i].bid_won,
                     username:data2[i].username,
                     id:data2[i].id,
                     rank:0,
                 }

             }


             for (var i = 0; i < data3.length; i++) {
                 if(!dataObj[data3[i].id] ) dataObj[data3[i].id]={}

                 if( !dataObj[data3[i].id].bid_pending) dataObj[data3[i].id].bid_pending=0
                 if( !dataObj[data3[i].id].bid_won) dataObj[data3[i].id].bid_won=0

                 dataObj[data3[i].id] = {
                     bid_pending: dataObj[data3[i].id].bid_pending,
                     bid_lost:data3[i].bid_lost,
                     bid_won:dataObj[data3[i].id].bid_won,
                     username:data3[i].username,
                     id:data3[i].id,
                     rank:0,
                 }

             }
             for(var k in dataObj){
             data.push(dataObj[k])
            }


             if (e) {
                 conn.destroy();
                 ;
                 return reject(e);
             }
             else {
                 conn.destroy();
                 ;
                 resolve(data)
             }
         })
     })
 })
                     }

            })
        })
    },
    userBidStatus:(id)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ; return reject(err);;
                 } else {
                     conn.query('SELECT count(bids.bid_status) as bid_count,bid_status FROM `bids` ' +
                         ' where user_id=? group by bid_status ', [id], (e, data, fields) => {
                         var bid = {
                             pending: 0,
                             won: 0,
                             lost: 0
                         };
                         conn.release

                         if (data) {
                             for (var i = 0; i < data.length; i++) {
                                 if (data[i].bid_status == 'Pending') {
                                     bid.pending = data[i].bid_count
                                 }
                                 if (data[i].bid_status == 'Lost') {
                                     bid.lost = data[i].bid_count
                                 }
                                 if (data[i].bid_status == 'Won') {
                                     bid.won = data[i].bid_count
                                 }
                             }
                             return resolve(bid)
                         } else {
                             return reject(bid)
                         }
                     })
                 }
             })
        })
    },
    save:(bids)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     var fields = [];
                     var value = [];
                     for (var k in bids) {
                         fields.push('`' + k + '`')
                         value.push("'" + bids[k] + "'");
                     }
                     conn.query('INSERT INTO bids (' + fields.join(',') + ') values (' + value.join(',') + ')', function (error, results, fields) {
                         ;
                         if (results) {
                             conn.destroy(); ;
                             return resolve(results.insertId);
                         }
                         else{
                             conn.destroy(); ;
                             return reject(false);
                         }
                     });
                 }
             })
        });
    },
    update:(id,matchId,teamId)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('UPDATE matches SET coins=?, price=?, currency= ? WHERE id = ?',
                         [match.coins, match.price, match.currency, id]
                         , function (error, results, fields) {
                             ;
                             if (!error)
                             {conn.destroy(); ; return resolve(true);}
                             else{
                                 conn.destroy(); ;
                                 return reject(error);}
                         })
                 }
             })
        });
    },
    delete:(user,matchId)=>{
        return new Promise((resolve, reject) => {
            mysql.pool(function (err, conn) {
                if (err) {
                    conn.destroy(); ;return reject(err);;
                } else {

                    var userId = user.id;
                    conn.query('select * from bids where user_id =? and  match_id =? ', [userId, matchId], function (error, exists, fields) {

                        if (exists.length == 0) {
                            return reject(false)
                        }

                        conn.query('delete from bids where user_id =? and  match_id =? ', [userId, matchId], function (error, results, fields) {
                            if (results) {
                                var coins = 0;
                                if (config.onBid) coins = config.onBid;
                                console.log("user.coins = " + user.coins)
                                console.log("coins = " + coins)
                                user.coins = parseInt(user.coins) + parseInt(coins);
                                console.log("user.coins = " + user.coins)
                                userModel.updateCoins(user.coins, userId).then(() => {
                                }).catch(() => {
                                })
                                conn.destroy(); ; return resolve(true);
                            }
                            else {
                                conn.destroy(); ;
                                return reject(error);
                            }
                        })
                    })
                }
            })
        })
    },
}
export default Bids;
