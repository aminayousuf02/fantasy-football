/**
 * Created by ali on 5/27/18.
 */
/**
 * Created by ali on 5/24/18.
 */
/**
 * Created by ali on 5/22/18.
 */

import mysql from '../config/db';

const Team = {

    getAll: () => {
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('select s.*,sp.sport from teams as s left join sports as sp on s.sport_id=sp.id', (e, data, fields) => {
                         conn.destroy(); ;
                         if (data)
                             return resolve(data);
                         else
                             return reject()
                     })
                 }
             })
        })
    },
    getAllBySportId: (sport_id) => {
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('select s.*,sp.sport from teams as s left join sports as sp on s.sport_id=sp.id where s.sport_id=' + sport_id, (e, data, fields) => {
                         conn.destroy(); ;
                         if (data)
                             return resolve(data);
                         else
                             return reject()
                     })
                 }
             })
        })
    },
    addTeam:(team,series_id)=>{
         mysql.pool(function (err, conn) {
             if (err) {
                 conn.destroy(); ;return reject(err);;
             } else {
                 conn.query('INSERT INTO team_series (`series_id`,`team_id`) values (' + series_id + ',' + team + ')', function (error, results, fields) {
                     conn.destroy(); ;

                 });
             }
         })
    },
    teamAddOnLeague:(teams,series_id)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('DELETE FROM `team_series` WHERE `series_id` = ' + series_id, (e, data, fields) => {
                         conn.destroy(); ;
                         if (data) {
                             for (var i = 0; i < teams.length; i++) {
                                 Team.addTeam(teams[i], series_id)
                             }
                             return resolve()

                         } else
                             return reject()
                     })
                 }
             })
        })
    },

    getAllBytournamentId: (series_id) => {
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('select s.*,sp.sport,sr.title as league from teams as s ' +
                         'left join sports as sp on s.sport_id=sp.id ' +
                         'inner join team_series as ts on ts.team_id=s.id ' +
                         'inner join series as sr on sr.id=ts.series_id ' +
                         'where ts.series_id=' + series_id, (e, data, fields) => {
                         conn.destroy(); ;
                         if (data)
                             return resolve(data);
                         else
                             return reject()
                     })
                 }
             })
        })
    },
    getOne:(id)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     conn.query('select s.*,sp.sport from teams as s left join sports as sp on s.sport_id=sp.id where s.id =' + id, (e, data, fields) => {
                         conn.destroy(); ;
                         if (data)
                             return resolve(data[0]);
                         else
                             return reject()
                     })
                 }
             })
        })

    },
    save:(league)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {
                     var fields = [];
                     var value = [];
                     for (var k in league) {
                         fields.push('`' + k + '`')
                         value.push("'" + league[k] + "'");
                     }
                     conn.query('INSERT INTO teams (' + fields.join(',') + ') values (' + value.join(',') + ')', function (error, results, fields) {
                         conn.destroy(); ;
                         if (results)
                             return resolve(results.insertId);
                         else return reject(false);
                     });
                 }
             })
        });
    },
    update:(id,team)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {

                     conn.query('UPDATE teams SET name=?, sport_id= ?, image= ?, country= ?  WHERE id = ?',
                         [team.name, team.sport_id, team.image, team.country, id]
                         , function (error, results, fields) {
                             conn.destroy(); ;
                             if (!error)return resolve(true);
                             else return reject(false);
                         })
                 }
             })
        });
    },
    delete:(id)=>{
        return new Promise((resolve, reject) => {
             mysql.pool(function (err, conn) {
                 if (err) {
                     conn.destroy(); ;return reject(err);;
                 } else {

                     conn.query('delete from teams where id = ' + id, function (error, results, fields) {
                         conn.destroy(); ;
                         if (results) return resolve(true);
                         else return reject(false);
                     });
                 }
             })
        });
    },
}
export default Team;
