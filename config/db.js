/**
 * Created by ali on 5/22/18.
 */
var mysql = require('mysql');
import CONSTANT from './constants';
console.log("Databace = "+CONSTANT.db.database)

//connection.connect();
// promise implementation
var mysqlPromise = require('promise-mysql');


var getConnection = function (cb) {
    var ppool = mysqlPromise.createPool({
        host: CONSTANT.db.server,
        user: CONSTANT.db.username,
        password: CONSTANT.db.password,
        database: CONSTANT.db.database,
        connectionLimit: 15,
        queueLimit: 30,
        acquireTimeout: 5000
    });
    ppool.getConnection(function (err, connection) {
        if(err) {
            return cb(err);
        }
        cb(null, connection);
    });
};

function getSqlConnection(){
    var ppool = mysqlPromise.createPool({
        host: CONSTANT.db.server,
        user: CONSTANT.db.username,
        password: CONSTANT.db.password,
        database: CONSTANT.db.database,
        connectionLimit: 1, // Default value is 10.
    });
    return ppool.getConnection().disposer(function(connection){
        ppool.releaseConnection(connection);
    });
}


export default {pool:getConnection}

